<?php
session_start();
include("../db.php");

if (isset($_SESSION["ADMIN_ID"])) {
                echo "<script>window.open('admin_home.php','_self')</script>";

}
?>
<!DOCTYPE html>
<html>

<head>
   <?php
include("stuffs.php");
?>

</head>


<body>
    <div class="admin_login_bg">
        <div class="container">
            <div class="row">
                <div class="col s12 m3"></div>
                <div class="col s12 m6 ">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="box ">
                        <h3 class="primary_heading center">XMPLARZ</h3>
                        <form action="" method="post">
                            <div class="tab_user" id="tab_user">
                                <!-- <p class="secondary_paragraph">Enter Your Credentials</p> -->
                                <div class="input-field">
                                    <input id="user_name" type="text" name="username" class="validate" required>
                                    <label for="user_name">User Name</label>

                                </div>
                                <span id="warning" class="red-text">Please Enter your Username</span><br>
                                <span class="btn waves-effect btn-small waves-light" id="nxt_btn">Next
                                    <i class="material-icons right ">send</i>
                                </span>

                            </div>
                            <div class="tab_pwd" id="tab_pwd">
                                <div class="usernamewelcome center">
                                    <h5 class="secondary_heading center">Welcome</h5>
                                    <span id="username" class="text-center usernamee">Praveenrambalu</span>
                                </div>
                                <div class="input-field">
                                    <input id="password" type="password" name="password" class="validate" required>
                                    <label for="password">Password</label>
                                </div>
                                <button class="btn waves-effect btn-small waves-light" type="submit" name="submit" id="submit_btn">Submit
                                    <i class="material-icons right ">lock_open</i>
                                </button>
                            </div>
                        </form>
                        <br>
                        <br>
                        <br>

                        <span class="developer">Developed by <a href="http://praveenrambalu.tk">Praveenram Balachandran</a></span>
                    </div>
                </div>
                <div class="col s12 m3"></div>
            </div>
        </div>
    </div>
<?php
if (isset($_POST["submit"])) {
    $username=$_POST["username"];
    $password=$_POST["password"];
    $password=md5($password);
    $sql="SELECT * FROM symposium_admin WHERE SYMPO_ADMIN_USERNAME='$username' AND SYMPO_ADMIN_PASS='$password'";
			$res=$db->query($sql);
			if($res->num_rows>0)
			{
				$row=$res->fetch_assoc(); 
				$_SESSION["ADMIN_ID"]=$row["SYMPO_ADMIN_ID"];
                $_SESSION["ADMIN_USERNAME"]=$row["SYMPO_ADMIN_USERNAME"];
                $sql="UPDATE symposium_admin SET SYMPO_ADMIN_LASTLOGIN = NOW() WHERE symposium_admin.SYMPO_ADMIN_USERNAME = '$username'";
                $db->query($sql);
                echo "<script>window.open('admin_home.php','_self')</script>";
                
			}
			else
			{
        echo '<script>swal("Invalid Credentials !", "Username or Password is Wrong please contact the Developer", "error");</script>';
				
			}
}
?>
</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax ").parallax();
        $(".tooltipped ").tooltip();
        $('.sidenav').sidenav();
        $('.carousel').carousel();
        $('#query').characterCounter();

        $("#warning").hide();
        $("#tab_pwd").hide();
        // $("#warning").hide();
        $("#nxt_btn").click(function() {
            var a = $("#user_name").val();
            if (a == "" || a == null) {
                $("#warning").show(1000);
            } else {
                $("#tab_user").hide();
                $("#tab_pwd").show(1000);
                $("#username").text(a);

            }

        });

    });
</script>

</html>