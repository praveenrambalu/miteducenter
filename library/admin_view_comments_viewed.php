<?php
	include("../db.php");
	include("../functions.php");
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login as student');
    }
    else {
        $adminid=$_SESSION["ADID"];
    }
    
   if (isset($_GET["page"])) {
    $page=$_GET["page"];
    $limit=($page*10)-10;

}
else{
    $page=1;
    $limit=0;
}
 $prev=$page-1;
$next=$page+1;
 if($prev<=0){
 $prev=1;
 }
 $nextpage=" <a href='admin_view_comments.php?page=$next' class='btn btn-success nocolor'>Next   <span class='fa fa-arrow-right'></span> </a>";
 if($page==1){
      $previous="";
 }
 else{
      $previous=" <a href='admin_view_comments.php?page=$prev' class='btn btn-primary nocolor'><span class='fa fa-arrow-left'></span>  Previous</a>";

 } 


?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">


                        <?php include("lib_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10 ">
        
			<div class="blur-box box ">
            <div class="pull-right">
        <a href="admin_view_comments.php" class="btn btn-primary nocolor"><span class="fa fa-eye"></span></a>
        </div>
            <?php
            if (isset($_GET["mes"])) {
                $mes=$_GET["mes"];
                echo "<br><div class='alert alert-success alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>$mes</strong>
                        </div><br>";
            }
 

?>
            <!-- <div class="blur-box">
            <form action="student_search_book.php" method="GET" autocomplete="off">
               <div class="input-group ">
                <input type="search" class="form-control" name="KEY" required placeholder="Search" id=""/>
                <div class="input-group-btn ">
                        <button class="btn btn-primary" type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                        </button>
                </div>
                </div>
            </form>
            </div> -->
           
      <?php
$sql="SELECT * FROM bookcomments WHERE COMMENT_STATUS='VIEWED' order by COMMENT_ID DESC LIMIT $limit,10";
// echo $sql;
$res=$db->query($sql);
		
		if($res->num_rows>0)
		{
            echo '<div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Book Name</th>
                        <th>Username</th>
                        <th>Comments</th>
                        <th>View</th>
                        <th>Delete</th>
                     
                 </tr>
             </thead>
    <tbody>';
            $i=1;
			while($row=$res->fetch_assoc())
				{
$bookid=$row["COMMENT_BOOK_ID"];
$commentid=$row["COMMENT_ID"];
                    echo "<tr>
                    <td>$i</td>
                    <td>{$row["COMMENT_BOOK_NAME"]}</td>
                    <td>{$row["COMMENT_STUDENT_NAME"]}</td>
                    <td>{$row["COMMENT_COMMENTS"]}</td>
                    <td><a href='admin_view_book_seperate.php?bookid=$bookid' class='btn btn-primary nocolor'><span class='fa fa-eye'></span></a></td>
                     <td><a href='admin_delete_comment.php?commentid=$commentid&ref=viewed' class='btn btn-danger nocolor'><span class='fa fa-trash'></span></a></td>
                    </tr>";
                    $i++;
                }
            }
            else{
               echo "<br><br><div class='alert alert-success alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>No Record Found..!</strong>
                        </div><br>"; 
            }
?>
    </tbody>
            </table>
           </div>
             
                
       <?PHP  
          echo $previous;
        echo $nextpage;    
                   
              ?>
                
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
   

</body>


</html>