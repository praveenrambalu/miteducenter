<?php 	
include"db.php";
session_start();
	if(!isset($_SESSION["STUID"]))
	{
		header('Location:index.php?mes=please login as student');
    }
    else {
        $stuid=$_SESSION["STUID"];
    }
    ?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg7"">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">
<?php
$stuid=$_SESSION["STUID"];
$sql="SELECT * FROM studentprofile WHERE STUID=$stuid;";
$res=$db->query($sql);
if($res->num_rows>0)
 {
 while($row=$res->fetch_assoc())
 {
   $name=$row["NAME"];
      $rollno=$row["ROLLNO"];
      $regno=$row["REGNO"];
      $dob=$row["DOB"];
 }
}
?>
                        <li> <a href="student_home.php" class="active"><span style="color:#fff;">Welcome </span> <?php echo $name; ?></a></li>
<?php include("student_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>
    <div class="container">
        <div class="row fs">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 blur-box ">
              
                 <?php
                $sql="SELECT * FROM post WHERE POST_STATUS='ACTIVATED' ORDER BY POST_ID DESC limit 0,5";
                  $res=$db->query($sql);
                if($res->num_rows>0)
                    {
                    while($row=$res->fetch_assoc())
                    {
                        $POST_ID=$row["POST_ID"];
                        $POST_TITLE=$row["POST_TITLE"];
                        $POST_DESCRIPTION=$row["POST_DESCRIPTION"];
                        $POST_FROM=$row["POST_FROM"];
                        $POST_TO=$row["POST_TO"];
                        $POST_VENUE=$row["POST_VENUE"];
                        $POST_CONTACT=$row["POST_CONTACT"];
                        $POST_MAIL=$row["POST_MAIL"];
                        $POST_INCHARGE=$row["POST_INCHARGE"];
                        $POST_REGLINK=$row["POST_REGLINK"];
                        $POST_IMAGE=$row["POST_IMAGE"];
                       if ($POST_CONTACT!="") {
                           $CONTACT="<div class='btn-group '>
                    <button type='button' class='btn btn-success nocolor '>Contact: $POST_CONTACT</button>
                    </div>";
                       }
                       else {
                           $CONTACT="";
                       }
                        if ($POST_MAIL!="") {
                           $MAIL="<div class='btn-group '>
                    <button type='button' class='btn btn-success nocolor '>Mail: $POST_MAIL</button>
                    </div>";
                       }
                       else {
                           $MAIL="";
                       }   if ($POST_REGLINK!="") {
                           $LINK="<div class='btn-group'>
                    <button type='button' class='btn btn-danger nocolor'>Link :<a href='$POST_REGLINK' class='text-white'>Visit</a></button>
                    </div>";
                       }
                       else {
                           $LINK="";
                       }
                        echo "
                        <div class='col-sm-12 outerbox '>
                    <div class='col-sm-3 '>
                        <img src='$POST_IMAGE' alt='$POST_TITLE' class='imgsec img img-responsive img-thumbnail' data-toggle='modal' data-target='#previewimg'>
                    </div>
                    <div class='col-sm-9 ' >
                        <h3 class='posttitle'>$POST_TITLE</h3>
                        <p class='postdescription'>$POST_DESCRIPTION</p>
                       
                        <div class='border-box3 pull-right'>
                            <b>Date : </b> $POST_FROM $POST_TO<br>
                            <b>Venue : </b>$POST_VENUE<br>
                            <b>Incharge : </b> $POST_INCHARGE
                        </div>  
                    
                    </div>
                    <div class='col-sm-12'>
                    <div class='btn-group btn-group-justified techmono'>

                    $CONTACT $MAIL $LINK
                    </div>
                    <br>
                    </div>
                </div>
                       ";
                    }
                }



                ?>
                
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>

    </div>

        <div class="modal fade" id="previewimg" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="" alt="Image" id="modalimg" width="100%">
                    </div>
                    <div class="modal-footer">
                        <h3 id="footermodal"></h3>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
<script>
        $("document").ready(function() {
            $(".imgsec").click(function() {
                var a = $(this).attr("src");
                var b = $(this).attr("alt");
                $("#modalimg").attr("src", a);
                $("#modalimg").attr("alt", b);
                $("#footermodal").html(b)

            });
        });
    </script>


</body>


</html>