<?php
	include "db.php";
	session_start();

	unset ($_SESSION["STUID"]);
	unset ($_SESSION["ADID"]);
	session_destroy();
	header('Location:index.php');
?>