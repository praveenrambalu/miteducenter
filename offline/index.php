<!DOCTYPE html>
<html>

<head>
   <?php include("db.php"); include("stuffs.php"); include("sms.php");?>
     <?php 
  
$sql="SELECT * FROM authenticationkey";
$res=$db->query($sql);
if($res->num_rows>0)
 {
 while($row=$res->fetch_assoc())
 {
     $authkey=$row["KEYAUTH"];
     $key=$authkey;
}
}
// $authkey='240994A9Nf11Y6kQ5bb5be3b';
  ?>
</head>

<body>

           <nav id="nav" class="text-uppercase">
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo">XMPLARZ</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li class="active"><a href="index.php">home</a></li>

                    <li  ><a href="view.php">view</a></li>
                    <!-- <li><a href="Vie.php">Vie</a></li> -->
                </ul>
            </div>
        </nav>

        <ul class="sidenav text-uppercase" id="mobile-demo">
                    <li class="active"><a href="index.php">home</a></li>
                    <li><a href="view.php">view</a></li>
          

           
        </ul>
   
   

 
    <div class="container">
        <div class="row">
           
            <h5 class="secondary_heading wow slideInLeft ">Register Here :</h5>
            <div class="row">
                <form class="col s12" action="" method="POST">
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="name" name="name" type="text" class="validate" required>
                            <label for="name">Name *</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">contact_mail</i>
                            <input id="email" name="email" type="email" class="validate" required>
                            <label for="email">Email *</label>
                        </div>
                   </div>
                    <div class="row">
                        
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">contact_phone</i>
                            <input id="phone" name="phone" type="number" class="validate" required>
                            <label for="phone">Phone *</label>
                        </div>
                          <div class="input-field col s12 m6">
                            <i class="material-icons prefix">place</i>
                            <input id="college" type="text" name="college" class="validate autocompletecollege" required>
                            <label for="college">College Name *</label>
                        </div>
                    </div>
                    
                    <div class="row">

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">domain</i>
                            <select name="department" required>
<option value="" disabled >Choose your Department</option>

                                <option value="CSE">CSE</option>
                                <option value="IT">IT</option>
                                <option value="ECE">ECE</option>
                                <option value="EEE">EEE</option>
                                <option value="BIO">Bio</option>
                            </select>
                            <label>Department</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">format_list_numbered</i>
                            <select name="year" required>
<option value="" disabled >Choose your year</option>

                                <option value="First Year">First Year</option>
                                <option value="Second Year">Second Year</option>
                                <option value="Third Year">Third Year</option>
                                <option value="Final Year">Final Year</option>
                                
                            </select>
                            <label>Year</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">insert_invitation</i>

                            <select id="event" name="event1" required>
        <option value="" disabled>Choose 1 Event</option>
        <option value="Paper Presentation">Paper Presentation</option>
        <option value="Code Debugging">Code Debugging</option>
        <option value="Online Quiz">Online Quiz</option>
        <option value="Advertisement">Advertisement</option>
    </select>
                            <label>Event </label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">insert_invitation</i>

                            <select id="event2" name="event2" required>
        <option value="" disabled>Choose 2 Event</option>
        
        <option value="Code Debugging">Code Debugging</option>
        <option value="Online Quiz">Online Quiz</option>
        <option value="Advertisement">Advertisement</option>
    </select>
                            <label>Event </label>
                        </div>



                    </div>
                    <div class="row">
                 

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">assignment</i>
                            <select name="paper">
        <option value="" disabled>Choose your Paper</option>

        <option value="">Not Participating</option>
        <option value="Virtual Reality">Virtual Reality</option>
        <option value="Augmented Reality">Augmented Reality</option>
        <option value="Machine Learning">Machine Learning</option>


    </select>
                            <label>Paper</label>
                        </div>

                           <div class="col s12 m6 ">
                            <label>Gender</label>
                            <label>
                               
<input class="with-gap" name="gender" value="male" checked type="radio" />
                            <span>Male</span>
                        </label>
                            <label>
                               
                    <input class="with-gap" name="gender" value="female"  type="radio" />
                            <span>Female</span>
                        </label>
                        </div>
                    </div>
                    <input type="submit" value="Register" name="register" class="btn ">

                </form>
            </div>

        </div>
    </div>
<?php
if (isset($_POST["register"])) {
    $MAIL=$_POST["email"];
  $sql="SELECT * FROM symposium_registration WHERE SYMPO_REG_MAIL='$MAIL'";
//   echo $sql;
$res=$db->query($sql);
if($res->num_rows>0)
{
    $Exists="MAIL EXISTS";
        echo "<script>M.toast({html: '$Exists', classes: 'rounded'}); </script>";
        // echo '<script>swal("Mail Exists!", "Sorry your mail already exists any doubt please contact the coordinator!", "warning");</script>';

}
else{
     $sql="INSERT INTO symposium_registration (SYMPO_REG_ID, SYMPO_REG_NAME,  SYMPO_REG_MAIL, SYMPO_REG_PHONE, SYMPO_REG_GENDER, SYMPO_REG_COLLEGE, SYMPO_REG_DEPARTMENT, SYMPO_REG_YEAR, SYMPO_REG_EVENT1, SYMPO_REG_EVENT2, SYMPO_REG_PAPER, SYMPO_REG_LOG) VALUES (NULL, '{$_POST["name"]}', '{$_POST["email"]}', '{$_POST["phone"]}', '{$_POST["gender"]}', '{$_POST["college"]}', '{$_POST["department"]}', '{$_POST["year"]}', '{$_POST["event1"]}', '{$_POST["event2"]}', '{$_POST["paper"]}', NOW());";

    if($db->query($sql))
    {
         $success="Registered Successfully";  
        echo "<script>M.toast({html: '$success', classes: 'rounded'}); </script>";
    //   echo "posted";  
    $receiver=$_POST["phone"];
    $name=$_POST["name"];

     $message='Hi,'.$name.'\n Your Name sucessfully';
    //  echo $receiver;
    //  echo $message;
    //  echo $key;
   sms($receiver,$key,$message);

    } 
    else{
        $failure="FAILED";
        echo "<script>M.toast({html: '$failure', classes: 'rounded'}); </script>";
       
    } 
}
}
?>

</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax").parallax();
        $(".tooltipped").tooltip();
        $('.sidenav').sidenav();
        $('.carousel').carousel();
        $('select').formSelect();
        $('#abstract').characterCounter();

        $('input.autocompletecollege').autocomplete({
            data: {
                "AVS Engineering College": null,
                "Annapoorana Engineering College": null,
                "Sona College of Technology": null,
                "Vinayaka Missions Kirupananda Variyar Engineering College": null,
                "VSA Engineering College": null,
                "Government College of Engineering, Salem": null,
                "Knowledge Institute of Technology": null,
                "Mahendra College of Engineering": null,
                "Mahendra Engineering College": null,
                "Shri Sakthikailassh Womens College": null,
                "Salem College of Engineering and Technology": null,
                "Narasus Sarathy Institute of Technology": null,
                "Sri Shanmugha College of Engineering, Sankari, Salem": null,
                "K.S.R Educational Institutions": null,
                "Mahendra Institute of Technology": null,

            },
        });
        $("#eventbtn").click(function() {
            var a = $("#event").val();
            alert(a);
        });


    });
</script>

</html>