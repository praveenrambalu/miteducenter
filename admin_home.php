<?php 	
include "db.php";
include "functions.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:index.php?mes=please login');
    }
    ?>
<!DOCTYPE html>
<html>

<head>
    <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg2">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbaradmin" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="navbaradmin">
                    <ul class="nav navbar-nav navbar-right text-uppercase">
                        
<?php include("admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>

<div class="container ">
   <div class="row fs blur-box">
        <div class="col-sm-2"></div>
        <div class="col-sm-8 table-responsive">
            <table class="table table-stripped text-white">
                <?php
if (countRecord("SELECT * from bookcomments WHERE COMMENT_STATUS='NOT-VIEWED'",$db)>0) {
    $badge='<span class="badge badge-primary">NEW</span>';
}
else {
    $badge="";
}

?>

			<tr><a href="admin_view_book.php"><th>Total Books : </th><td> <?php echo countRecord("SELECT * from book",$db); ?></td> </a></tr>
			<tr><a href="admin_view_comments.php"><th>Total Comments    : </th><td><?php echo countRecord("SELECT * from bookcomments",$db); ?></td> </a></tr>
			<tr><a href="admin_seen_comment"> <th>Viewed comment  : </th><td><?php echo countRecord("SELECT * from bookcomments WHERE COMMENT_STATUS='VIEWED'",$db); ?></td> </a></tr>
			<tr><a href="admin_view_commens.php"> <th>UnViewed Comments : </th><td><?php echo countRecord("SELECT * from bookcomments WHERE COMMENT_STATUS='NOT-VIEWED'",$db); echo "     \t".$badge; ?></td> </a></tr>
			<tr><a href="../student/admin_view_student.php"> <th>Total Students: </th><td><?php echo countRecord("SELECT * from studentprofile",$db); ?></td> </a></tr>
			<tr> <th>Total Posts: </th><td><?php echo countRecord("SELECT * from post",$db); ?></td> </a></tr>
			<tr> <th>Inactive Posts: </th><td><?php echo countRecord("SELECT * from post where POST_STATUS='INACTIVE'",$db); ?></td> </a></tr>
			
		</table>


        </div>
        <div class="col-sm-2"></div>
    </div>
</div>

    </div>




</body>


</html>