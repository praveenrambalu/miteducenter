<?php
	include("../db.php");
session_start();
	if(!isset($_SESSION["STUID"]))
	{
		header('Location:../index.php?mes=please login as student');
    }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("mes_student_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>

<?php
$studentid=$_SESSION["STUID"];
$sql="SELECT * FROM studentprofile WHERE STUID=$studentid;";
 $res=$db->query($sql);
if($res->num_rows>0)
{
    while($row=$res->fetch_assoc())
    {
        $studentname=$row["NAME"];
        $studentid=$row["STUID"];
        $photo=$row["PHOTO"];
    }
}

?>
<div class="container ">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10 fs">
<div class="blur-box-non-rounded" id="messagecontainer"></div>
<br>
<div class="blur-box">
    <div class="status"></div>
    <form name="gchats" class="" id="gchatform">
        <input type="hidden" value="<?php echo $studentid; ?>" id="usid"name="uid">
        <input type="hidden" value="<?php echo $studentname; ?>" id="name"name="name">
        <input type="hidden" value="<?php echo $photo; ?>" id="photo"name="photo">
        <input autocomplete="off" style="width:80%;" type="text" name="gmsg" id="gmsg"  class="pull-left form-control"placeholder="Type the Message here..." autofocus>
        <span id="gchatsubmit"  style="width:20%;" class=" btn btn-block  nocolor btn-primary pull-right"> <span class="fa fa-send "></span></span>
        <br><br>
    </form>
</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
   

</body>
<script>
      $("document").ready(function(){
      $("#messagecontainer").load('chat_loader.php');
    
  setInterval(function(){
      $("#messagecontainer").load('chat_loader.php');
    }, 3000);

$("#gchatsubmit").on("click",function(){
var id=$("#usid").val();
var mes=$("#gmsg").val();
var name=$("#name").val();
var photo=$("#photo").val();

$.post("chat_post.php",{uid:id,gmsg:mes,name:name,photo:photo},function(data){
$("#status").html(data);
$("#gmsg").val("");
});
});

$("#gchatform").on("submit",function(){
var id=$("#usid").val();
var mes=$("#gmsg").val();
var name=$("#name").val();
var photo=$("#photo").val();

$.post("chat_post.php",{uid:id,gmsg:mes,name:name,photo:photo},function(data){
$("#status").html(data);
$("#gmsg").val("");
});
});
});

function functione(){
$('#messagecontainer').scrollTop($('#messagecontainer')[0].scrollHeight);
}
</script>

</html>