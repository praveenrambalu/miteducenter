<?php 	include"../db.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
     if(!isset($_GET["eventid"])){
        header('Location:admin_view_event.php');
    }
    else{
        $eventid=$_GET["eventid"];

    }
    ?>
<!DOCTYPE html>
<html>

<head>
    <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg2">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("fb_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>
<?php 
$sql="SELECT * FROM eventdetail WHERE EVID=$eventid";
 $res=$db->query($sql);

if($res->num_rows>0)
{
while($row=$res->fetch_assoc())
{
    $eventname=$row["EV_NAME"];
   $eventdescription=$row["EV_DESCRIPTION"];
   $eventdate=$row["EV_DATE"];
   $eventtime=$row["EV_TIME"];
   $eventorganizer=$row["EV_ORGANIZER"];
   $eventresourseperson=$row["EV_RESOURCEPERSON"];
   $eventcoordinator=$row["EV_COORDINATOR"];
   $eventclass=$row["EV_CLASS"];
   $eventvenue=$row["EV_VENUE"];
}
}
?>
        <div class="container">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box">
                        <div class="blurbox">
                            <form action="" method="post" autocomplete="off">
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Name</label>
                                    <input type="text" class="form-control" value="<?php echo $eventname; ?>" name="EV_NAME">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Description</label>
                                    <textarea type="text" class="form-control" name="EV_DESCRIPTION"><?php echo $eventdescription; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Date</label>
                                    <input type="date" class="form-control"  value="<?php echo $eventdate; ?>" name="EV_DATE">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Time</label>
                                    <input type="time" class="form-control" value="<?php echo $eventtime; ?>"  name="EV_TIME">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Organizer</label>
                                    <input type="text" class="form-control"  value="<?php echo $eventorganizer; ?>" name="EV_ORGANIZER">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Resource Person</label>
                                    <input type="text" class="form-control" value="<?php echo $eventresourseperson; ?>"  name="EV_RESOURCEPERSON">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Cooridnator</label>
                                    <input type="text" class="form-control"  value="<?php echo $eventcoordinator; ?>" name="EV_COORDINATOR">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Class</label>
                                    <input type="text" class="form-control"  value="<?php echo $eventclass; ?>" name="EV_CLASS">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Venue</label>
                                    <input type="text" class="form-control" value="<?php echo $eventvenue; ?>"  name="EV_VENUE">
                                </div>
                                <input type="submit" name="updated" value="update" class="btn btn-primary">
                                <br>
                                <br>
                                <br>
                                <br>

                            </form>
                            <?php
if(isset($_POST["updated"])){
   $editedeventname=$_POST["EV_NAME"];
   $editedeventdescription=$_POST["EV_DESCRIPTION"];
   $editedeventdate=$_POST["EV_DATE"];
   $editedeventtime=$_POST["EV_TIME"];
   $editedeventorganizer=$_POST["EV_ORGANIZER"];
   $editedeventresourseperson=$_POST["EV_RESOURCEPERSON"];
   $editedeventcoordinator=$_POST["EV_COORDINATOR"];
   $editedeventclass=$_POST["EV_CLASS"];
   $editedeventvenue=$_POST["EV_VENUE"];

$sql ="UPDATE eventdetail SET EV_NAME='$editedeventname', EV_DATE='$editedeventdate',EV_DESCRIPTION='$editedeventdescription',EV_TIME='$editedeventtime',EV_ORGANIZER='$editedeventorganizer',EV_RESOURCEPERSON='$editedeventresourseperson',EV_COORDINATOR='$editedeventcoordinator',EV_CLASS='$editedeventclass',EV_VENUE='$editedeventvenue' WHERE EVID=$eventid";
    if($db->query($sql))
				{
				echo '<script>alert("Good Job..! edited");</script>';
				// echo '<script>swal("Good Job..!","The Category '.$category.' added ..","success");</script>';
				}
				else
				{
				// echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
				echo '<script>alert("Sorry ! Some error occured please try after some time");</script>';
				
				}

}
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
    </div>



</body>


</html>