<?php 	
include"db.php";
session_start();
	if(!isset($_SESSION["STUID"]))
	{
		header('Location:index.php?mes=please login as student');
    }
    else {
        $stuid=$_SESSION["STUID"];
    }
    ?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg4"">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">
<?php
$stuid=$_SESSION["STUID"];
$sql="SELECT * FROM studentprofile WHERE STUID=$stuid;";
$res=$db->query($sql);
if($res->num_rows>0)
 {
 while($row=$res->fetch_assoc())
 {
   $name=$row["NAME"];
      $rollno=$row["ROLLNO"];
      $regno=$row["REGNO"];
      $dob=$row["DOB"];
 }
}
?>
                        <li> <a href="student_home.php" class="active"><span style="color:#fff;">Welcome </span> <?php echo $name; ?></a></li>
<?php include("student_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>
    <div class="container">
        <div class="row fs">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="blur-box">
               <form action="" method="post">
                                <div class="form-group">
                                    <label for="" class="event-title-student">Event Name</label>
                                    <select name="event_name" class="form-control">
                                    <?php
                                    $sql="SELECT * FROM eventdetail ORDER BY EVID DESC";
                                    $res=$db->query($sql);
                                    if($res->num_rows>0)
                                      {
                                     while($row=$res->fetch_assoc())
                                      {
                                          $eventname=$row["EV_NAME"];
                                        echo"<option  value='$eventname'><i class='text-uppercase'>$eventname</i></option>";
                                       
                                      }
                                    }
                                    ?>
                                    
                                       
                                    </select>
                                <input type="submit" name="viewevent" value="VIEW" class="btn btn-primary"> 
                                </form>


                                  <?php
if(isset($_POST["viewevent"])){
    $vieweventname=$_POST["event_name"];

    $sql="SELECT eventquestions.QUID, eventdetail.EVID,eventdetail.EV_NAME, eventdetail.EV_DATE, eventdetail.EV_DESCRIPTION,eventdetail.EV_TIME,eventdetail.EV_ORGANIZER,eventdetail.EV_RESOURCEPERSON,eventdetail.EV_COORDINATOR,eventdetail.EV_CLASS,eventdetail.EV_VENUE,eventquestions.QUESTION FROM eventdetail INNER JOIN eventquestions ON eventdetail.EVID=eventquestions.EVENT_ID WHERE eventdetail.EV_NAME='$vieweventname'";
 $res=$db->query($sql);
                                         $i=1;
                                         $qidd=1;
$sl=1;
                                    if($res->num_rows>0)
                                      {
                                     while($row=$res->fetch_assoc())
                                      {
                                          $evid=$row["EVID"];
                                          $qid=$row["QUID"];
                                          $evname=$row["EV_NAME"];
                                          $evdate=$row["EV_DATE"];
                                          $evdescription=$row["EV_DESCRIPTION"];
                                          $evtime=$row["EV_TIME"];
                                          $evorganizer=$row["EV_ORGANIZER"];
                                          $evresourceperson=$row["EV_RESOURCEPERSON"];
                                          $evcoordinator=$row["EV_COORDINATOR"];
                                          $evclass=$row["EV_CLASS"];
                                          $evvenue=$row["EV_VENUE"];
                                          $evquestion=$row["QUESTION"];
                                         if($i==1){
                                             echo"<div class='text-center'><h1 class='view-event-title  text-uppercase'>$evname</h1></div>";
                                             echo"<div class='row'>
                                             <div class='col-sm-4'><div class='pull-left'><p><b>Resource person:</b> $evresourceperson<br> <b>Coordinator:</b> $evcoordinator<br><b>Organizer:</b> $evorganizer</p></div></div>
                                             <div class='col-sm-4'></div>
                                             <div class='col-sm-4'><div class='pull-right'><p><b>Date: </b>$evdate<br> <b>Time: </b>$evtime<br><b>Venue:</b> $evvenue<br><b>Class:</b> $evclass</p></div></div>
                                             </div>";
                                             echo"
                                             <h4 class='view-desc-heading'>Description : </h4>
                                             <p class='view-desc'>$evdescription</p> </div>";
                                             echo"<div class='blur-box box'><h4 class='view-desc-heading'>Question : </h4>";
                                             echo"<form action='' method='POST'>";
                                             $i++;
                                         }
                                            
                                            echo"$sl ) $evquestion <br>";
                                            echo"<input type='hidden' value='$evid' name='eventid'>";
                                            echo"<input type='hidden' value='$qid' name='qid$sl'>";
                                            echo"<br>";
                                          echo"
<label class='radio-inline'><input type='radio' name='qs$qidd' value='5'>
Excellent
</label>
<label class='radio-inline'><input type='radio' name='qs$qidd' value='4'>
Good
</label>
<label class='radio-inline'><input type='radio' name='qs$qidd' value='3'>
Better
</label>
<label class='radio-inline'><input type='radio' name='qs$qidd' value='2'>
Average
</label>
<label class='radio-inline'><input type='radio' name='qs$qidd'  value='1'>
Unsatifactory
</label> <br><br>";

                                            
                                          $sl++;
                                          $qidd++;
                                      }
                                    }
                                    $count=$sl-1;
                                            echo"<input type='hidden' value='$count' name='count'>";
                                    
                                    echo"<input type='submit' class='btn btn-success' name='submit' value='submit' >";
}
echo'</form>';
   ?>
   <?php
if(isset($_POST["submit"])){
    // print_r($_POST);
    $fbeventid=$_POST["eventid"];
    
$sql="SELECT * FROM fbans WHERE FB_EVENTID=$fbeventid AND FB_STUID=$stuid";
 $res=$db->query($sql);
if($res->num_rows>0)
{
 echo"<script>alert('you have already registered  for this event thanks for coming')</script>";
}
else{





    $count=$_POST["count"];
   
for ($a=1;$a<=$count;$a++) {

$fbquestionid=$_POST["qid$a"];
$fbquestionanswer=$_POST["qs$a"];
$sql="INSERT INTO fbans ( FB_EVENTID, FB_QUID, FB_ANS, FB_STUID) VALUES ( '$fbeventid', '$fbquestionid', '$fbquestionanswer', '$stuid');";
// echo $sql;
if($db->query($sql))
				{
				
				}
				else
				{
				// echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
				echo '<script>alert("Sorry ! Some error occured please try after some time");</script>';
				
				}
				


}
}
}
    ?>
</div>

    </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>

    </div>



</body>


</html>