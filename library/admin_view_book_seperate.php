<?php
	include("../db.php");
	include("../functions.php");
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login as ADMIN');
    }
    else {
        $adminid=$_SESSION["ADID"];
    }


    if (!isset($_GET["bookid"])) {
       echo "<script>window.open('admin_view_book.php','_self')</script>";
    }
    else{
        $bookid=$_GET["bookid"];
    }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>

<?php

$sql="SELECT * FROM facultyprofile WHERE FAID=$adminid;";
$res=$db->query($sql);
if($res->num_rows>0)
 {
 while($row=$res->fetch_assoc())
 {
   $name=$row["NAME"];

 }
}
?>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("lib_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10 ">
			<div class="blur-box container col-sm-12 box">
                   <?php
            if (isset($_GET["mes"])) {
                $mes=$_GET["mes"];
                echo "<br><div class='alert alert-success alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>$mes</strong>
                        </div><br>";
            }
 

?>
                <div class="blur-box container col-sm-12">
                   
                          
                        
            <?php 
	
		$sql="SELECT * FROM book where BOOK_ID=$bookid";
		$res=$db->query($sql);
		
		if($res->num_rows>0)
		{
			while($row=$res->fetch_assoc())
				{
                    $BOOK_ID=$row["BOOK_ID"];
                    $BOOK_URL=$row["BOOK_URL"];
                    $BOOK_NAME=$row["BOOK_NAME"];
                    $BOOK_DEPT=$row["BOOK_DEPT"];
                    $BOOK_YEAR=$row["BOOK_YEAR"];
                    $BOOK_AUTHOR=$row["BOOK_AUTHOR"];
                    $BOOK_IMAGE=$row["BOOK_IMAGE"];
                    $BOOK_UPLOADER=$row["BOOK_UPLOADER"];
                    $TIMEANDDATE=$row["TIMEANDDATE"];
                    $BOOK_KEYWORDS=$row["BOOK_KEYWORDS"];
                    $TYPES=$row["TYPES"];
                    echo "<div class='col-sm-3'>
                            <img src='$BOOK_IMAGE' alt='' class='img img-responsive img-thumbnail'>
                        </div>
                    <div class='col-sm-9'>
                        <div class=''>
                        <table class='table table-noborder' >";
                    echo " <tr><th class='text-uppercase'>Title</th> <td> : </td> <td>$BOOK_NAME</td></tr>
                           <tr><th class='text-uppercase'>Author </th> <td> : </td> <td>$BOOK_AUTHOR</td></tr>
                           <tr><th class='text-uppercase'>DEPT</th> <td> : </td> <td>$BOOK_DEPT</td></tr>
                           <tr><th class='text-uppercase'>YEAR</th> <td> : </td> <td>$BOOK_YEAR</td></tr>
                           <tr><th class='text-uppercase'>uploader</th> <td> : </td> <td>$BOOK_UPLOADER</td></tr>";
             
                }
                echo '      </table>
                        </div>
                    </div>
                </div>
            ';
            
        }
		else
		{
			echo "<p class='text-danger'>No Book Record Found</p>";
        }
        ?>
        <div class='preview_btn_holder'>
            <div class='btn-group btn-group-justified'>
            <a href='' data-toggle="modal" data-target="#previewModal" class='btn btn-primary nocolor'><span class='fa fa-eye'></span> </a>
            <a href='<?php echo $BOOK_URL ?>' class='btn btn-success nocolor'><span class='fa fa-download'></span> </a>
            <a href='admin_edit_book.php?bookid=<?php echo $BOOK_ID ?>' class='btn btn-default nocolor'><span class='fa fa-edit'></span> </a>
            <a href='admin_delete_book.php?bookid=<?php echo $BOOK_ID ?>&ref=seperate' class='btn btn-danger nocolor'><span class='fa fa-trash'></span> </a>
            <!-- <a href='student_add_book_comment.php?bookid=$BOOK_ID' class='btn btn-default nocolor'><span class='fa fa-comment'></span> </a> -->
        </div>

        <br>
        <div class="blur-box">
            <form action="" autocomplete="off" method="POST">
                <textarea placeholder="comment about this book..!" name="comment" required class="form-control" style="resize: none;"></textarea>
                <br>
                <input type="submit" value="Comment" name="submit" class="btn btn-primary nocolor">
            </form>
            <br>
            <br>
            <div class="blur-box comment-box">


            <?php
            $sql="SELECT * FROM bookcomments WHERE COMMENT_BOOK_ID=$BOOK_ID ORDER BY COMMENT_ID DESC";
                $res=$db->query($sql);
                if($res->num_rows>0)
                {
                    while($row=$res->fetch_assoc())
                    {
                        $COMMENT_STUDENT_NAME=$row["COMMENT_STUDENT_NAME"];
                        $COMMENT_LOG=$row["COMMENT_LOG"];
                        $COMMENT_COMMENTS=$row["COMMENT_COMMENTS"];
                        $COMMENT_LOG=time_elapsed_string($COMMENT_LOG);
                        echo " <div class='border-box'>
                        <span class='comment_name text-uppercase'>$COMMENT_STUDENT_NAME</span>
                        <span class='comment_time pull-right'>$COMMENT_LOG</span>
                        
                        <div class='border-box2'>
                        <p>$COMMENT_COMMENTS</p>
                        </div>
                        </div>
                        <br>";
                    }
                }
                else{
                    echo "<div class='border-box'>No Record Found</div>";
                }
                ?>
               
            </div>
        </div>
                <?php
                $svg=' <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer" x="0px" y="0px" width="20" height="20" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve">
<circle fill="#CCE4FF" cx="32" cy="32" r="16"/>
<path fill="#007AFF" d="M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z"/>
<path fill="#007AFF" d="M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z"/>
<path fill="#007AFF" d="M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z"/>
</svg>  ';
                $name2=$name.$svg;


// echo $name2;
                if (isset($_POST["submit"])) {
                  $COMMENTS=$_POST["comment"];
                  
                    $sql="INSERT INTO bookcomments (COMMENT_STUDENT_NAME, COMMENT_BOOK_ID, COMMENT_BOOK_NAME, COMMENT_LOG, COMMENT_STATUS, COMMENT_COMMENTS) VALUES ('$name2', '$bookid', '$BOOK_NAME', CURRENT_TIMESTAMP, 'VIEWED', '$COMMENTS');";
                    if($db->query($sql))
	                    {
                     echo "<script>window.open('admin_view_book_seperate.php?bookid=$bookid','_self')</script>";

                        }
                }
    
                ?>
      
               </div> 
			</div>
		</div>
		<div class="col-sm-1"></div>
   
    </div>
</div>
  



<div id="previewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-black"><?php echo $BOOK_NAME ?></h4>
      </div>
      <div class="modal-body">
       <?php
switch ($TYPES) {
    case 'FILE':
    echo "<iframe src='http://docs.google.com/gview?url=http://estudylib.tk/library/$BOOK_URL&embedded=true' style='width:100%; height:500px;' frameborder='0'></iframe>";
    break;
    case 'LINK':
      echo '<iframe src=" <?php echo $file;?>/preview" style="height:500px; width:100%;">No Preview Available</iframe>';
        
        break;
    
    default:
      ECHO "LINK&FILE";
        break;
}

        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>

<script>
jQuery(document).ready(function() {
  jQuery("time.timeago").timeago();
});
</script>
</html>