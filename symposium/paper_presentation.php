<!DOCTYPE html>
<html>

<head>
     <?php
include("../db.php");
include("stuffs.php");



?>

</head>


<body>

    <nav id="nav" class="text-uppercase">
        <div class="nav-wrapper">
            <a href="#!" class="brand-logo">XMPLARZ</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="home.php">home</a></li>

                <li><a href="register.php">register</a></li>
                <li><a href="aboutus.php">aboutus</a></li>
            </ul>
        </div>
    </nav>

    <ul class="sidenav text-uppercase" id="mobile-demo">
        <li><a href="home.php">home</a></li>

        <li><a href="register.php">register</a></li>
        <li><a href="aboutus.php">aboutus</a></li>
    </ul>


    <div class="parallax-container">
        <div class="parallax"><img src="img/0257dee24a37ef2.jpg">
        </div>
        <h1 class="events_primary_heading center wow  slideInUp  events_heading_centerized" style=" color: #26C6DA; ">PREZENTAZY</h1>
    </div>
    <div class="row section ">
        <div class="container ">
            <h3 class="primary_heading wow bounceInLeft ">
                <a class=" tooltipped " data-position="bottom " data-tooltip="Paper Presentation ">Paper Presentation</a>
            </h3>
            <p class="text-justify paragraph-indent primary_paragraph wow bounceInRight ">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente quae placeat ipsam est nulla et voluptates accusamus? Doloremque, quo ipsam. Beatae fugiat modi explicabo cum doloribus debitis exercitationem, laudantium accusamus. </p>
            <hr class="wow rotateIn ">
            <h5 class="secondary_heading wow slideInLeft ">Note :</h5>
            <ol class="primary_paragraph wow slideInRight ">
                <li>Send your papers to xmplarz@outlook.com on or before August 26th.</li>
                <li>Confirmation of your paper will be send on August 28th.</li>
                <li>Accomodation will be available for outside participants.</li>
                <li>Food and Travel will be take care by Participants.</li>
            </ol>
            <hr class="wow roateIn ">
            <h5 class="secondary_heading wow slideInLeft ">Presentation Topics :</h5>
            <ol class="primary_paragraph wow slideInRight ">
                <li>Virtual Reality</li>
                <li>Augmented Reality</li>
                <li> Machine Learning</li>
                <li>Neural Network</li>
                <li> Recent Trending Topics in IT</li>
            </ol>
            <hr class="wow roateIn ">
            <h5 class="secondary_heading wow slideInLeft ">Rules :</h5>
            <ol class="primary_paragraph wow slideInRight ">
                <li>Maximum 3 members in a team. </li>
                <li>Paper must be in IEEE format (Not Slide).</li>
                <li>Timing 8 minutes for Presentation and 2 Minutes for Quries.</li>
                <li>Dress code must be formal.</li>
                <li>Participants should submit their paper on before August 26th.</li>
                <li>Spot registration also available.</li>
            </ol>
            <hr class="wow roateIn ">
            <h5 class="secondary_heading wow slideInLeft ">Any Queries :</h5>
            <div class="row">
                <form class="col s12" autocomplete="off" method="POST" action="">
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="name" type="text" name="name" class="validate" required>
                            <label for="name">Full Name *</label>
                        </div>
                        <div class="input-field col m6 s12">
                            <i class="material-icons prefix">contact_mail</i>
                            <input id="email" type="email" name="email" class="validate" required>
                            <label for="email">Email *</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <i class="material-icons prefix">contact_phone</i>
                            <input id="phone" type="number" name="phone" class="validate">
                            <label for="phone">Phone</label>
                            <input type="hidden" name="event" value="Paper Presentation">
                        </div>
                        <div class="input-field col m6 s12">
                            <i class="material-icons prefix">question_answer</i>
                            <textarea id="query" class="materialize-textarea" name="query" required data-length="300"></textarea>
                            <label for="query">Query *</label>
                        </div>
                        <div class="col m12 s12">
                            <button class="btn waves-effect waves-light  col s12" type="submit" name="submit">Submit
                                <i class="material-icons right">send</i>
                                </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col s6">
                <a href="fun_games.php" class="btn waves-effect waves-light right blue pulse"><i class="material-icons right">keyboard_arrow_left</i></a>
            </div>
            <div class="col s6">
                <a href="code_debugging.php" class="btn waves-effect waves-light green pulse"><i class="material-icons right">keyboard_arrow_right</i></a>

            </div>
        </div>
    </div>

<?php
if(isset($_POST["submit"])) {
   
   $sql="INSERT INTO symposium_queries(SYMPO_QUERY_ID, SYMPO_QUERY_NAME, SYMPO_QUERY_MAIL, SYMPO_QUERY_PHONE, SYMPO_QUERY_EVENT, SYMPO_QUERY_QUERY, SYMPO_QUERY_STATUS) VALUES (NULL, '{$_POST["name"]}', '{$_POST["email"]}', '{$_POST["phone"]}', '{$_POST["event"]}', '{$_POST["query"]}', 'NOT SOLVED');";
    if($res=$db->query($sql)){
        echo '<script>swal("Good job!", "We will Contact you ASAP!", "success");</script>';
    }

}

?>
</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax ").parallax();
        $(".tooltipped ").tooltip();
        $('.sidenav').sidenav();
        $('.carousel').carousel();
        $('#query').characterCounter();

    });
</script>

</html>