<?php
	include("../db.php");
	include("../functions.php");
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login as student');
    }
    else {
        $adminid=$_SESSION["ADID"];
    }
      if (!isset($_GET["bookid"])) {
       echo "<script>window.open('admin_view_book.php','_self')</script>";
    }
    else{
        $bookid=$_GET["bookid"];
    }
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">


                        <?php include("lib_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10 ">
			<div class="blur-box box ">
<?php
$sql="SELECT * FROM book WHERE BOOK_ID=$bookid";
$res=$db->query($sql);
		
		if($res->num_rows>0)
		{
			while($row=$res->fetch_assoc())
				{
                    $BOOK_ID=$row["BOOK_ID"];
                    $BOOK_URL=$row["BOOK_URL"];
                    $BOOK_NAME=$row["BOOK_NAME"];
                    $BOOK_DEPT=$row["BOOK_DEPT"];
                    $BOOK_YEAR=$row["BOOK_YEAR"];
                    $BOOK_AUTHOR=$row["BOOK_AUTHOR"];
                    $BOOK_IMAGE=$row["BOOK_IMAGE"];
                    $BOOK_UPLOADER=$row["BOOK_UPLOADER"];
                    $TIMEANDDATE=$row["TIMEANDDATE"];
                    $BOOK_KEYWORDS=$row["BOOK_KEYWORDS"];
                }
        }
?>
           <?php
	if(isset($_POST["submit"]))
		{
            
                    $name=$_POST["name"];
                    $dept=$_POST["dept"];
                    $year=$_POST["year"];
                    $author=$_POST["author"];
                    $keywords=$_POST["keywords"];
                    $book=$_POST["book"];
                    $coverimage=$_POST["coverimage"];
                    $sql="UPDATE book SET BOOK_NAME = '$name', BOOK_DEPT = '$dept', BOOK_YEAR = '$year', BOOK_AUTHOR = '$author', BOOK_KEYWORDS = '$keywords',BOOK_IMAGE = '$coverimage', BOOK_URL = '$book' WHERE book.BOOK_ID = $bookid;";  
                      if($db->query($sql))
				        {
		                     echo "<br><div class='alert alert-success alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>The Book $name Edited succesfully</strong>
                        </div><br>";
                        }   
                        else
                        {
                            echo "<br><div class='alert alert-danger alert-dismissible fade in'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                        <strong>Error Edit the File</strong>
                                    </div><br>";
                        }       
                }
		
?>
            
            <form action="" autocomplete="off" method="post" enctype="multipart/form-data">
                 <legend>Book</legend>
                    <div class="form-group"> 
                        <label for="name">Book Name:</label>
                        <input type="text" required name="name" class="form-control" value="<?php echo $BOOK_NAME ?>" id="">
                    </div> 
                    <div class="form-group"> 
                        <label for="dept">Department</label>
                         <input type="text" required name="dept" class="form-control" value="<?php echo $BOOK_DEPT ?>" id="">

                    </div> 
                    <div class="form-group"> 
                        <label for="year">Year</label>
                        <input type="text" required name="year" class="form-control" value="<?php echo $BOOK_YEAR ?>" id="">

                    </div> 
                    <div class="form-group"> 
                        <label for="author">Author Name: (optional)</label>
                        <input type="text" name="author" class="form-control" value="<?php echo $BOOK_AUTHOR ?>" placeholder="Eg: J. Stanley Jayaprakash" id="">
                    </div> 
                    <div class="form-group"> 
                        <label for="coverimage">Book Cover  Image (optional)</label>
                        <input type="text" name="coverimage" class="form-control" value="<?php echo $BOOK_IMAGE ?>" placeholder="Eg: http://drive.google.com/file/u/1/234343">
                    </div> 
                    <div class="form-group"> 
                        <label for="book">Book </label>
                        <input type="text"  name="book" class="form-control" value="<?php echo $BOOK_URL ?>" required placeholder="Eg: http://drive.google.com/file/u/1/234343">
                    </div> 
                    <div class="form-group"> 
                        <label for="keywords">Keywords </label>
                        <textarea type="text" name="keywords" class="form-control" required><?php echo $BOOK_KEYWORDS ?></textarea>
                    </div> 
                
                        <input type="submit" name="submit" class="btn btn-block btn-success" value="Edit book">
               
                </form>
     
      
                
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
   

</body>


</html>