<?php
	include("../db.php");
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("post_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>

<?php
$admin_id=$_SESSION["ADID"];
$sql="SELECT * FROM facultyprofile WHERE FAID=$admin_id;";
 $res=$db->query($sql);
if($res->num_rows>0)
{
    while($row=$res->fetch_assoc())
    {
        $admin_name=$row["NAME"];
    }
}

?>
<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="blur-box box">
                 <div class="pull-right">
                    <a href="index.php" class="btn btn-primary"><span class="fa fa-file"></span></a>
                </div>
                <?php
	if(isset($_POST["submit"]))
		{
            
           
                
                    $posttitle=$_POST["posttitle"];
                    $postdescription=$_POST["postdescription"];
                    $postfrom=$_POST["postfrom"];
                    $postto=$_POST["postto"];
                    $postvenue=$_POST["postvenue"];
                    $postcontact=$_POST["postcontact"];
                    $postmail=$_POST["postmail"];
                    $postincharge=$_POST["postincharge"];
                    $postlink=$_POST["postlink"];
                    $postimglink=$_POST["postimglink"];
                  
                    
                    $sql="INSERT INTO post (POST_ID, POST_TITLE, POST_DESCRIPTION, POST_FROM, POST_TO, POST_VENUE, POST_CONTACT, POST_MAIL, POST_INCHARGE, POST_UPLOADER, POST_LOG, POST_REGLINK, POST_IMAGE, POST_STATUS) VALUES (NULL, '$posttitle', '$postdescription', '$postfrom', '$postto', '$postvenue', '$postcontact', '$postmail', '$postincharge', '$admin_name', CURRENT_TIMESTAMP, '$postlink', '$postimglink', 'ACTIVATED');";  
                    // echo $sql;  
                    if($db->query($sql))
				        {
		                     echo "<br><div class='alert alert-success alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>The Post Added succesfully</strong>
                        </div><br>";
                        } 
                        			
                        else
                        {
                            echo "<br><div class='alert alert-danger alert-dismissible fade in'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                        <strong>Error Uploading the Post</strong>
                                    </div><br>";
                        }         
                
			
		
	
		}
?>

                <form action="" autocomplete="off" method="post" enctype="multipart/form-data">
                 <legend>Post</legend>
                    <div class="form-group"> 
                        <label for="posttitle">Post Title:</label>
                        <input type="text" required name="posttitle" class="form-control"  id="">
                    </div> 
                     <div class="form-group"> 
                        <label for="postdescription">Description </label>
                        <textarea type="text" name="postdescription" class="form-control" required></textarea>
                    </div>
                    <div class="form-group"> 
                        <label for="postfrom">From:</label>
                        <input type="date" required name="postfrom" class="form-control"  id="">
                    </div>
                    <div class="form-group"> 
                        <label for="postto">To:</label>
                        <input type="date" required name="postto" class="form-control"  id="">
                    </div>
                     <div class="form-group"> 
                        <label for="postvenue">Venue:</label>
                        <input type="text" required name="postvenue" class="form-control"  id="">
                    </div> 
                      <div class="form-group"> 
                        <label for="postcontact">contact:</label>
                        <input type="text"  name="postcontact" class="form-control"  id="">
                    </div> 
                      <div class="form-group"> 
                        <label for="postmail">Mail:</label>
                        <input type="text"  name="postmail" class="form-control"  id="">
                    </div> 
                      <div class="form-group"> 
                        <label for="postincharge">Incharge:</label>
                        <input type="text"  name="postincharge" class="form-control"  id="">
                    </div> 
                      <div class="form-group"> 
                        <label for="postlink">Link:</label>
                        <input type="url"  name="postlink" class="form-control"  id="">
                    </div> 
                      <div class="form-group"> 
                        <label for="postimglink">Image:</label>
                        <input type="url" name="postimglink" class="form-control">
                    </div> 
                  
                    
                        <input type="submit" name="submit" class="btn btn-block btn-success nocolor" value="Add Post">
               
                </form>
                
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
   

</body>


</html>