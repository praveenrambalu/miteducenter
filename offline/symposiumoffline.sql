-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2018 at 05:36 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `symposiumoffline`
--

-- --------------------------------------------------------

--
-- Table structure for table `authenticationkey`
--

CREATE TABLE `authenticationkey` (
  `ID` int(11) NOT NULL,
  `KEYAUTH` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authenticationkey`
--

INSERT INTO `authenticationkey` (`ID`, `KEYAUTH`) VALUES
(1, '252462AY2fjxta5c19065a');

-- --------------------------------------------------------

--
-- Table structure for table `symposium_registration`
--

CREATE TABLE `symposium_registration` (
  `SYMPO_REG_ID` int(11) NOT NULL,
  `SYMPO_REG_NAME` varchar(50) NOT NULL,
  `SYMPO_REG_MAIL` text NOT NULL,
  `SYMPO_REG_PHONE` varchar(30) NOT NULL,
  `SYMPO_REG_GENDER` varchar(10) NOT NULL,
  `SYMPO_REG_COLLEGE` text NOT NULL,
  `SYMPO_REG_DEPARTMENT` text NOT NULL,
  `SYMPO_REG_YEAR` varchar(15) NOT NULL,
  `SYMPO_REG_EVENT1` text NOT NULL,
  `SYMPO_REG_EVENT2` text NOT NULL,
  `SYMPO_REG_PAPER` varchar(30) NOT NULL,
  `SYMPO_REG_LOG` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authenticationkey`
--
ALTER TABLE `authenticationkey`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `symposium_registration`
--
ALTER TABLE `symposium_registration`
  ADD PRIMARY KEY (`SYMPO_REG_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authenticationkey`
--
ALTER TABLE `authenticationkey`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `symposium_registration`
--
ALTER TABLE `symposium_registration`
  MODIFY `SYMPO_REG_ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
