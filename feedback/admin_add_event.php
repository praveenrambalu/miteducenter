<?php 	include"../db.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }?>
<!DOCTYPE html>
<html>

<head>
    <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg2">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("fb_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>

        <div class="container">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box">
                        <div class="blurbox">
                            <form action="" method="post" autocomplete="off">
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Name</label>
                                    <input type="text" class="form-control" name="EV_NAME">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Description</label>
                                    <textarea type="text" class="form-control" name="EV_DESCRIPTION"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Date</label>
                                    <input type="date" class="form-control" name="EV_DATE">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Time</label>
                                    <input type="time" class="form-control" name="EV_TIME">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Organizer</label>
                                    <input type="text" class="form-control" name="EV_ORGANIZER">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Resource Person</label>
                                    <input type="text" class="form-control" name="EV_RESOURCEPERSON">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Cooridnator</label>
                                    <input type="text" class="form-control" name="EV_COORDINATOR">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Class</label>
                                    <input type="text" class="form-control" name="EV_CLASS">
                                </div>
                                <div class="form-group">
                                    <label for="" class="reglabel">Venue</label>
                                    <input type="text" class="form-control" name="EV_VENUE">
                                </div>
                                <input type="submit" name="evregistersubmit" value="REGISTER" class="btn btn-primary">
                                <br>
                                <br>
                                <br>
                                <br>

                            </form>
                            <?php
if(isset($_POST["evregistersubmit"])){
   $eventname=$_POST["EV_NAME"];
   $eventdescription=$_POST["EV_DESCRIPTION"];
   $eventdate=$_POST["EV_DATE"];
   $eventtime=$_POST["EV_TIME"];
   $eventorganizer=$_POST["EV_ORGANIZER"];
   $eventresourseperson=$_POST["EV_RESOURCEPERSON"];
   $eventcoordinator=$_POST["EV_COORDINATOR"];
   $eventclass=$_POST["EV_CLASS"];
   $eventvenue=$_POST["EV_VENUE"];

$sql ="INSERT INTO eventdetail (EV_NAME, EV_DATE, EV_DESCRIPTION, EV_TIME, EV_ORGANIZER, EV_RESOURCEPERSON, EV_COORDINATOR, EV_CLASS, EV_VENUE) VALUES ('$eventname','$eventdate','$eventdescription','$eventtime','$eventorganizer','$eventresourseperson','$eventcoordinator','$eventclass','$eventvenue')";
    if($db->query($sql))
				{
				echo '<script>alert("Good Job..! Registered");</script>';
				// echo '<script>swal("Good Job..!","The Category '.$category.' added ..","success");</script>';
				}
				else
				{
				// echo '<script>swal("Sorry !","Some Error Occured. Please try after some time","error");</script>';
				echo '<script>alert("Sorry ! Some error occured please try after some time");</script>';
				
				}

}
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
    </div>



</body>


</html>