<!DOCTYPE html>
<html>

<head>
   <?php include("db.php"); include("stuffs.php");?>

</head>

<body>

           <nav id="nav" class="text-uppercase">
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo">XMPLARZ</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="index.php">home</a></li>

                    <li  class="active" ><a href="view.php">view</a></li>
                    <!-- <li><a href="Vie.php">Vie</a></li> -->
                </ul>
            </div>
        </nav>

        <ul class="sidenav text-uppercase" id="mobile-demo">
                    <li><a href="index.php">home</a></li>
                    <li class="active"><a href="view.php">view</a></li>
          

           
        </ul>
   
   

 <br>
 <br>
 <br>

<div class="container">
 <form action="" method="post">
 <div class="row">
<div class="input-field col s6">
    <select name="event" >
      <option value="" disabled selected>Choose your option</option>
      <option value="c">Code Debugging</option>
      <option value="p">Paper Presentation</option>
      <option value="q">Quiz</option>
     
      
    </select>
    <label>Materialize Select</label>
  </div>
<div class="input-field col s6">
<input type="submit" value="Search" class="btn" >
<a href="view.php" class="btn "><i class="material-icons">dns</i></a>
</div>
</div>
</form>
  <div class="row">
    <div class="col s12">
    


<table id="myTable" class="centered"> 
     <!-- add materialize classes, as desired -->
  <thead class="blue">
    <tr class="">
      <th class="filter-select filter-exact" data-placeholder="">SNO</th>
      <th>Name </th>
      <th>Mail</th>
      <th>Phone</th>
      <th class="filter-select filter-exact" data-placeholder="">Sex</th>
      <th>College</th>
      <th>Department</th>
      <th>Year</th>
      <th>Event 1</th>
      <th>Event 2</th>
      <th>Paper</th>
      <th class="filter-select filter-exact" data-placeholder="">Log</th>
      <th class="filter-select filter-exact" data-placeholder="">Delete</th>
    </tr>
  </thead>
  <tfoot>
    <tr class="tablesorter-ignoreRow">
      <th colspan="15" class="ts-pager form-horizontal">
        <button type="button" class="btn first"><i class="small material-icons">first_page</i></button>
        <button type="button" class="btn prev"><i class="small material-icons">navigate_before</i></button>
        <span class="pagedisplay"></span>
        <!-- this can be any element, including an input -->
        <button type="button" class="btn next"><i class="small material-icons">navigate_next</i></button>
        <button type="button" class="btn last"><i class="small material-icons">last_page</i></button>
        <select class="pagesize browser-default" title="Select page size">
          <option selected="selected" value="10">10</option>
          <option value="20">20</option>
          <option value="30">30</option>
          <option value="40">40</option>
          <option value="1000">All</option>
          <!-- <option value="1">one</option> -->
        </select>
        <select class="pagenum browser-default" title="Select page number"></select>
        <!-- <button id="alerttest">testttttttttt</button> -->
      </th>
      
    </tr>
  </tfoot>
  <tbody>

  <?php
  if (isset($_POST["event"])) {
    $event=$_POST["event"];
  }
  else{
    $event="p";
  }
  $sql="SELECT * FROM symposium_registration WHERE SYMPO_REG_EVENT1 LIKE '%$event%' OR SYMPO_REG_EVENT2 LIKE '%$event%';";
  // echo $sql;
      $res=$db->query($sql);
if($res->num_rows>0)
 {
     $i=1;
 while($row=$res->fetch_assoc())
 {
 $id=$row["SYMPO_REG_ID"];
 $name=$row["SYMPO_REG_NAME"];
 $mail=$row["SYMPO_REG_MAIL"];
 $phone=$row["SYMPO_REG_PHONE"];
 $gender=$row["SYMPO_REG_GENDER"];
 $college=$row["SYMPO_REG_COLLEGE"];
 $department=$row["SYMPO_REG_DEPARTMENT"];
 $year=$row["SYMPO_REG_YEAR"];
 $event1=$row["SYMPO_REG_EVENT1"];
 $event2=$row["SYMPO_REG_EVENT2"];
 $paper=$row["SYMPO_REG_PAPER"];
 $log=$row["SYMPO_REG_LOG"];
 echo "<tr class='center'><td>$i</td><td>$name</td><td>$mail</td><td>$phone</td><td>$gender</td><td>$college</td><td>$department</td><td>$year</td><td>$event1</td><td>$event2</td><td>$paper</td><td>$log</td></td><td><a href='admin_student_delete.php?studentid=$id' class='btn btn red'><i class='material-icons'>delete_forever</i></a></td></tr>";
$i++;
 }
 }
 else{
     echo "no record found";
 }
  ?>
   

</tbody>
       
    </div>
  </div>
  </div>
</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax").parallax();
        $(".tooltipped").tooltip();
        $('.sidenav').sidenav();
        $('.carousel').carousel();
        $('select').formSelect();
        $('#abstract').characterCounter();

        $('input.autocompletecollege').autocomplete({
            data: {
                "AVS Engineering College": null,
                "Annapoorana Engineering College": null,
                "Sona College of Technology": null,
                "Vinayaka Missions Kirupananda Variyar Engineering College": null,
                "VSA Engineering College": null,
                "Government College of Engineering, Salem": null,
                "Knowledge Institute of Technology": null,
                "Mahendra College of Engineering": null,
                "Mahendra Engineering College": null,
                "Shri Sakthikailassh Womens College": null,
                "Salem College of Engineering and Technology": null,
                "Narasus Sarathy Institute of Technology": null,
                "Sri Shanmugha College of Engineering, Sankari, Salem": null,
                "K.S.R Educational Institutions": null,
                "Mahendra Institute of Technology": null,

            },
        });
        $("#eventbtn").click(function() {
            var a = $("#event").val();
            alert(a);
        });

$(function() {

  $("table").tablesorter({
    theme : "materialize",
    widgets : [ "filter" ],
  })
  .tablesorterPager({
container: $(".ts-pager"),
 cssGoto  : ".pagenum",
 removeRows: false,
  output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

  });

});


$("table").tableExport({

  // Displays table headers (th or td elements) in the <thead>
  headers: true,                    

  // Displays table footers (th or td elements) in the <tfoot>    
  footers: true, 

  // Filetype(s) for the export
  formats: ["csv"],           

  // Filename for the downloaded file
  fileName: "Xmplarz Registration",                         

  // Style buttons using bootstrap framework  
  bootstrap: true,

  // Automatically generates the built-in export buttons for each of the specified formats   
  exportButtons: true,                          

  // Position of the caption element relative to table
//   position: "bottom",                   

  // (Number, Number[]), Row indices to exclude from the exported file(s)
  ignoreRows: null,                             

  // (Number, Number[]), column indices to exclude from the exported file(s)              
  ignoreCols: null,                   

  // Removes all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s)     
  trimWhitespace: false ,

     

});
    });
</script>

</html>