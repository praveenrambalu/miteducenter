<?php
	include("../db.php");
	include("../functions.php");
session_start();
	if(!isset($_SESSION["STUID"]))
	{
		header('Location:../index.php?mes=please login as student');
    }
    else {
        $stuid=$_SESSION["STUID"];
    }
    if (!isset($_GET["KEY"])) {
        echo "<script>window.open('index.php','_self')</script>";
        # code...
    }
    else{
        $key=$_GET["KEY"];
    }
    if (isset($_GET["page"])) {
    $page=$_GET["page"];
    $limit=($page*10)-10;

}
else{
    $page=1;
    $limit=0;
}
 $prev=$page-1;
$next=$page+1;
 if($prev<=0){
 $prev=1;
 }
 $nextpage=" <a href='student_search_book.php?page=$next&KEY=$key' class='btn btn-success nocolor'>Next   <span class='fa fa-arrow-right'></span> </a>";
 if($page==1){
      $previous="";
 }
 else{
      $previous=" <a href='student_search_book.php?page=$prev&KEY=$key' class='btn btn-primary nocolor'><span class='fa fa-arrow-left'></span>  Previous</a>";

 }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">
<?php
$stuid=$_SESSION["STUID"];
$sql="SELECT * FROM studentprofile WHERE STUID=$stuid;";
$res=$db->query($sql);
if($res->num_rows>0)
 {
 while($row=$res->fetch_assoc())
 {
   $name=$row["NAME"];
      $rollno=$row["ROLLNO"];
      $regno=$row["REGNO"];
      $dob=$row["DOB"];
 }
}
?>
                        <li> <a href="student_home.php" class="active"><span style="color:#fff;">Welcome </span> <?php echo $name; ?></a></li>

                        <?php include("lib_student_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10 ">
			<div class="blur-box box ">
            <div class="blur-box">
            <form action="student_search_book.php" method="GET" autocomplete="off">
               <div class="input-group ">
                <input type="search" class="form-control" name="KEY" required placeholder="Search" id=""/>
                <div class="input-group-btn ">
                        <button class="btn btn-primary" type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                        </button>
                </div>
                </div>
            </form>
            </div>
            <br>
            <br>
            <?php 
	
        $sql="SELECT * FROM book WHERE BOOK_NAME like '%{$_GET["KEY"]}%' or BOOK_KEYWORDS like '%{$_GET["KEY"]}%' ORDER BY BOOK_ID DESC LIMIT $limit,10";
        // ECHO $sql;
		$res=$db->query($sql);
		
		if($res->num_rows>0)
		{
			while($row=$res->fetch_assoc())
				{
                    $BOOK_ID=$row["BOOK_ID"];
                    $BOOK_URL=$row["BOOK_URL"];
                    echo "<div class='blur-box'>
                         <div class='book_heading'>
                            <h4>{$row["BOOK_NAME"]}</h4>
                            <div class='pull-right'><span class='text-primary'>AUTHOR : </span> {$row["BOOK_AUTHOR"]}</div>
                         </div>   
                         <div class='keywords'>
                         <br>
                            <p class='bkeytitle'>Keywords :</p>";
                            	$key=$row["BOOK_KEYWORDS"];
							$spkey=keysplitter($key);
							$ikey=0;
							echo "<pre class='prekey'>";
							while($spkey)
							{
								echo "    <span class='fa fa-star keytext'></span> ";
								echo $spkey[$ikey];
								$ikey++;
								if($ikey==5)
								{
									break;
								}
							}
                            echo "</pre>
						</div>
                        <div class='preview_btn_holder'>
                             <div class='btn-group btn-group-justified'>
                                <a href='student_view_book.php?bookid=$BOOK_ID' class='btn btn-default nocolor'><span class='fa fa-eye'></span> </a>
                                <a href='$BOOK_URL' class='btn btn-default nocolor'><span class='fa fa-download'></span> </a>
                                <a href='student_view_book.php?bookid=$BOOK_ID' class='btn btn-default nocolor'><span class='fa fa-comment'></span> </a>
                            </div>
                        </div>
                    </div><br>";
                }
        }
		else
		{
			echo "<p class=' border-box'>We are upgrading You will get soooooooon ! stay connected.</p>";
		}?>
        
          <?PHP  
          echo $previous;
        echo $nextpage;    
                   
              ?>  
      
                
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
   

</body>


</html>