<?php
session_start();
include("../db.php");
if (!isset($_SESSION["ADMIN_ID"])) {
    header("Location:admin_login.php?mes=please login");
}
?>
<!DOCTYPE html>
<html>

<head>
    <?php
include("stuffs.php");
?>

</head>


<body>
    <?php include("admin_nav.php"); ?>
    <main>
        <div class="container">
            <div class="row">
<?php 
if (isset($_GET["mes"])) {
    $mes=$_GET["mes"];
 echo "<script>M.toast({html: '$mes', classes: 'rounded'}); </script>" ; 
 
}

?>



                <div class="col s12 m12 offset-m1  offset-xl1 offset-s1">
                    <h3 class="primary_heading">Change  Password</h3>
                 
                    <form action="" method="post" autocomplete="off" >
                    <div class="row">
                        <div class="input-field col s6">
                        <i class="material-icons prefix">lock</i>
                        <input id="icon_prefix" type="text" class="validate" name="opass">
                        <label for="icon_prefix">Old Password</label>
                        </div>
                     </div>
        
                    <div class="row">
                        <div class="input-field col s6">
                        <i class="material-icons prefix">lock</i>
                        <input id="icon_prefix" type="text" class="validate" name="npass">
                        <label for="icon_prefix">New Password</label>
                        </div>
                        <div class="input-field col s6">
                        <?php
if(isset($_POST["submit"]))
		{
			$opass=$_POST["opass"];
			$enopass=md5($opass);
			$sql="SELECT * FROM symposium_admin WHERE SYMPO_ADMIN_PASS='$enopass' and SYMPO_ADMIN_ID=".$_SESSION["ADMIN_ID"];
			$res=$db->query($sql);
			if($res->num_rows>0)
			{
				$npass=$_POST["npass"];
				$ennpass=md5($npass);
				$s="UPDATE symposium_admin SET SYMPO_ADMIN_PASS='$ennpass' WHERE ID=".$_SESSION["ADMIN_ID"];
				$db->query($s);
				echo "<script>M.toast({html: 'Password Changed Suceessfully', classes: 'rounded'}); </script>";
			}
			else
			{
				echo "<script>M.toast({html: 'Invalid Old Password', classes: 'rounded'}); </script>";
				
			}
		}
	?>
                        </div>
                    </div>

                    <div class="row">
                        <input type="submit" name="submit" class="btn cyan" value="Change">
                    </div>
                    </form>
 
                </div>
            </div>
        </div>
    </main>


</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax ").parallax();
        $(".tooltipped ").tooltip();
        $('.sidenav').sidenav();
        $(".sidenav").isFixed();
        $('.carousel').carousel();
        $('#query').characterCounter();
    
    });

</script>

</html>