<?php
session_start();
include("../db.php");
if (!isset($_SESSION["ADMIN_ID"])) {
    header("Location:admin_login.php?mes=please login");
}
?>
<!DOCTYPE html>
<html>

<head>
    <?php
include("stuffs.php");
?>

</head>


<body>
    <?php include("admin_nav.php"); ?>
    <main>
        <div class="container">
            <div class="row">
<?php 
if (isset($_GET["mes"])) {
    $mes=$_GET["mes"];
 echo "<script>M.toast({html: '$mes', classes: 'rounded'}); </script>" ; 
 
}

?>
                <div class="col s12 m12  offset-m1  offset-xl1 offset-s1">
                    <br>
                    
                    <h3 class="primary_heading">View Students</h3>
 <table id="myTable" class="centered"> 
     <!-- add materialize classes, as desired -->
  <thead>
    <tr class="">
      <th class="filter-select filter-exact" data-placeholder="">SNO</th>
      <th>Name </th>
      <th>Mail</th>
      <th>Phone</th>
      <th>Event</th>
      <th>Query</th>
      <th>Status</th>
      <th class="filter-select filter-exact" data-placeholder="">Delete</th>
      </tr>
  </thead>
  <tfoot>
    <tr class="tablesorter-ignoreRow">
      <th colspan="15" class="ts-pager form-horizontal">
        <button type="button" class="btn first"><i class="small material-icons">first_page</i></button>
        <button type="button" class="btn prev"><i class="small material-icons">navigate_before</i></button>
        <span class="pagedisplay"></span>
        <!-- this can be any element, including an input -->
        <button type="button" class="btn next"><i class="small material-icons">navigate_next</i></button>
        <button type="button" class="btn last"><i class="small material-icons">last_page</i></button>
        <select class="pagesize browser-default" title="Select page size">
          <option selected="selected" value="10">10</option>
          <option value="20">20</option>
          <option value="30">30</option>
          <option value="40">40</option>
          <option value="1000">All</option>
          <!-- <option value="1">one</option> -->
        </select>
        <select class="pagenum browser-default" title="Select page number"></select>
        <!-- <button id="alerttest">testttttttttt</button> -->
      </th>
      
    </tr>
  </tfoot>
  <tbody>

  <?php
  $sql="SELECT * FROM symposium_queries";
      $res=$db->query($sql);
if($res->num_rows>0)
 {
     $i=1;
 while($row=$res->fetch_assoc())
 {
 $id=$row["SYMPO_QUERY_ID"];
 $name=$row["SYMPO_QUERY_NAME"];
 $mail=$row["SYMPO_QUERY_MAIL"];
 $phone=$row["SYMPO_QUERY_PHONE"];
 $event=$row["SYMPO_QUERY_EVENT"];
 $query=$row["SYMPO_QUERY_QUERY"];
 $status=$row["SYMPO_QUERY_STATUS"];

 echo "<tr class='center'><td>$i</td><td>$name</td><td>$mail</td><td>$phone</td><td>$event</td><td>$query</td><td><a href='admin_query_status_approval.php?queryid=$id' class=''>$status</a></td><td><a href='admin_query_delete.php?queryid=$id' class='btn btn red'><i class='material-icons'>delete_forever</i></a></td></tr>";
$i++;
 }
 }
 else{
     echo "no record found";
 }
  ?>
   

</tbody>
       
                </div>
            </div>
        </div>
    </main>

</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax ").parallax();
        $(".tooltipped ").tooltip();
        $('.sidenav').sidenav();
        $(".sidenav").isFixed();
        $('.carousel').carousel();
        $('#query').characterCounter();
    
    });

$(function() {

  $("table").tablesorter({
    theme : "materialize",
    widgets : [ "filter" ],
  })
  .tablesorterPager({
container: $(".ts-pager"),
 cssGoto  : ".pagenum",
 removeRows: false,
  output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

  });

});


$("table").tableExport({

  // Displays table headers (th or td elements) in the <thead>
  headers: true,                    

  // Displays table footers (th or td elements) in the <tfoot>    
  footers: true, 

  // Filetype(s) for the export
  formats: ["csv"],           

  // Filename for the downloaded file
  fileName: "Query",                         

  // Style buttons using bootstrap framework  
  bootstrap: true,

  // Automatically generates the built-in export buttons for each of the specified formats   
  exportButtons: true,                          

  // Position of the caption element relative to table
//   position: "bottom",                   

  // (Number, Number[]), Row indices to exclude from the exported file(s)
  ignoreRows: null,                             

  // (Number, Number[]), column indices to exclude from the exported file(s)              
  ignoreCols: null,                   

  // Removes all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s)     
  trimWhitespace: false ,

     

});
</script>

</html>