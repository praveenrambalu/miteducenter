<!DOCTYPE html>
<html>

<head>
   <?php include("../db.php"); include("stuffs.php");?>
</head>

<body>

           <nav id="nav" class="text-uppercase">
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo">XMPLARZ</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="home.php">home</a></li>

                    <li  class="active"><a href="register.php">register</a></li>
                    <li><a href="aboutus.php">aboutus</a></li>
                </ul>
            </div>
        </nav>

        <ul class="sidenav text-uppercase" id="mobile-demo">
            <li><a href="home.php">home</a></li>

            <li  class="active"><a href="register.php">register</a></li>
            <li><a href="aboutus.php">aboutus</a></li>
        </ul>
   
    <div class="parallax-container">
        
        <div class="parallax"><img src="img/registration.png">

        </div>
    </div>

    <div class="row section  ">
        <div class="container ">
            <h3 class="primary_heading wow bounceInLeft"><a class=" tooltipped" data-position="bottom" data-tooltip="Mahendra Insitute of Technology">Mahendra Institute of Technology </a></h3>
            <p class="text-justify paragraph-indent primary_paragraph wow bounceInRight">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, autem obcaecati? Assumenda iure maxime nemo et unde, corrupti quia perspiciatis explicabo aperiam eveniet praesentium quasi, recusandae iste excepturi sint incidunt. Lorem ipsum
                dolor sit amet, consectetur adipisicing elit. Quis alias optio repudiandae corrupti tenetur pariatur officiis enim amet aliquid voluptatibus, cum laborum voluptatem similique nesciunt atque aperiam porro, perferendis vel.
            </p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <hr class="wow rotateIn ">

            <h5 class="secondary_heading wow slideInLeft ">General Instructions :</h5>
            <ol class="primary_paragraph wow slideInRight  justify">
                <li>Students should present inside the college campus on or before 9.00 A.M.</li>
                <li>The registration starts at 8.00 A.M. and sharply closed at 9.30 A.M.</li>
                <li>All the Participant should be present upto the valedictory function.</li>
                <li>Each and Every Participant should pay 150 for registration.</li>
                <li>Participants should bring college ID card .</li>
                <li>if any indisciplinary activity is noticed inside the campus, the concerned participants will be sent out from the college campus and informed to their respective college immediately. </li>
                <li>Minimum 2 Events you participant have to participate.</li>
                <li>Unauthorized students from other colleges will not be entertained.</li>
                <li>Accommodation will not be provided.</li>
                <li>The last date to submit the softcopy for paper presentation is on or before 25.01.2018. </li>
                <li>The intimation will be send on 31.01.2018.</li>

            </ol>
            <hr class="wow roateIn ">
            <h5 class="secondary_heading wow slideInLeft ">Locate Us :</h5>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3911.3986543806154!2d77.90040771473272!3d11.378564550901821!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba96192ffdf8f99%3A0xb9d24247ed3cb835!2sMahendra+Institute+of+Technology!5e0!3m2!1sen!2sao!4v1517651103897"
                width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
            <hr class="wow roateIn ">
            <h5 class="secondary_heading wow slideInLeft ">Contact Us :</h5>
            <ul class="collection">
                <li class="collection-item avatar">
                    <img src="img/team.jpg" alt="" class="circle">
                    <span class="title">Praveenram Balachandran</span>
                    <p><a href="tel:8220085613">Phone No: 8220085613</a><br><i>Overall Coordinator</i>
                    </p>
                    <a href="#!" class="secondary-content"><i class="material-icons">verified_user</i></a>
                </li>
                <li class="collection-item avatar">
                    <img src="img/team.jpg" alt="" class="circle">
                    <span class="title">Name</span>
                    <p><a href="tel:8220085613">Phone No: 8220085613</a><br><i>Overall Coordinator</i>
                    </p>
                    <a href="#!" class="secondary-content"><i class="material-icons ">verified_user</i></a>
                </li>
            </ul>


            <hr class="wow roateIn ">
            <h5 class="secondary_heading wow slideInLeft ">Register Here :</h5>
            <div class="row">
                <form class="col s12" action="" method="POST">
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="name1" name="name1" type="text" class="validate" required>
                            <label for="name1">Participant 1 Name *</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="name2" name="name2" type="text" class="validate">
                            <label for="name2">Participant 2 Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">contact_mail</i>
                            <input id="email" name="email" type="email" class="validate" required>
                            <label for="email">Email *</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">contact_phone</i>
                            <input id="phone" name="phone" type="number" class="validate" required>
                            <label for="phone">Phone *</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m6 ">
                            <label>Gender</label>
                            <label>
                               
<input class="with-gap" name="gender" value="male" checked type="radio" />
                            <span>Male</span>
                        </label>
                            <label>
                               
                    <input class="with-gap" name="gender" value="female"  type="radio" />
                            <span>Female</span>
                        </label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">place</i>
                            <input id="college" type="text" name="college" class="validate autocompletecollege" required>
                            <label for="college">College Name *</label>
                        </div>
                    </div>
                    <div class="row">

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">domain</i>
                            <select name="department" required>
<option value="" disabled >Choose your Department</option>

                                <option value="CSE">CSE</option>
                                <option value="IT">IT</option>
                                <option value="ECE">ECE</option>
                                <option value="EEE">EEE</option>
                                <option value="BIO">Bio</option>
                            </select>
                            <label>Department</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">format_list_numbered</i>
                            <select name="year" required>
<option value="" disabled >Choose your year</option>

                                <option value="First Year">First Year</option>
                                <option value="Second Year">Second Year</option>
                                <option value="Third Year">Third Year</option>
                                <option value="Final Year">Final Year</option>
                                
                            </select>
                            <label>Year</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">insert_invitation</i>

                            <select id="event" name="event1" required>
        <option value="" disabled>Choose 1 Event</option>
        <option value="Paper Presentation">Paper Presentation</option>
        <option value="Code Debugging">Code Debugging</option>
        <option value="Online Quiz">Online Quiz</option>
        <option value="Advertisement">Advertisement</option>
    </select>
                            <label>Event </label>
                        </div>
                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">insert_invitation</i>

                            <select id="event2" name="event2" required>
        <option value="" disabled>Choose 2 Event</option>
        <option value="Paper Presentation">Paper Presentation</option>
        <option value="Code Debugging">Code Debugging</option>
        <option value="Online Quiz">Online Quiz</option>
        <option value="Advertisement">Advertisement</option>
    </select>
                            <label>Event </label>
                        </div>



                    </div>
                    <div class="row">

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">assignment</i>
                            <select name="paper">
        <option value="" disabled>Choose your Paper</option>

        <option value="">Not Participating</option>
        <option value="Virtual Reality">Virtual Reality</option>
        <option value="Augmented Reality">Augmented Reality</option>
        <option value="Machine Learning">Machine Learning</option>


    </select>
                            <label>Paper</label>
                        </div>

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">question_answer</i>
                            <textarea id="abstract" class="materialize-textarea" name="abstract" data-length="500"></textarea>
                            <label for="abstract">Abstract </label>
                        </div>
                    </div>
                    <input type="submit" value="Register" name="register" class="btn ">

                </form>
            </div>

        </div>
    </div>
<?php
if (isset($_POST["register"])) {
    $MAIL=$_POST["email"];
  $sql="SELECT * FROM symposium_registration WHERE SYMPO_REG_MAIL='$MAIL'";
//   echo $sql;
$res=$db->query($sql);
if($res->num_rows>0)
{
        echo '<script>swal("Mail Exists!", "Sorry your mail already exists any doubt please contact the coordinator!", "warning");</script>';

}
else{
     $sql="INSERT INTO symposium_registration (SYMPO_REG_ID, SYMPO_REG_NAME1, SYMPO_REG_NAME2, SYMPO_REG_MAIL, SYMPO_REG_PHONE, SYMPO_REG_GENDER, SYMPO_REG_COLLEGE, SYMPO_REG_DEPARTMENT, SYMPO_REG_YEAR, SYMPO_REG_EVENT1, SYMPO_REG_EVENT2, SYMPO_REG_PAPER, SYMPO_REG_ABSTRACT, SYMPO_REG_STATUS, SYMPO_REG_LOG) VALUES (NULL, '{$_POST["name1"]}', '{$_POST["name2"]}', '{$_POST["email"]}', '{$_POST["phone"]}', '{$_POST["gender"]}', '{$_POST["college"]}', '{$_POST["department"]}', '{$_POST["year"]}', '{$_POST["event1"]}', '{$_POST["event2"]}', '{$_POST["paper"]}', '{$_POST["abstract"]}', 'NOT APPROVED', NOW());";

    if($db->query($sql))
    {
            $to=$MAIL; // Receiver Email ID, Replace with your email ID
            $NAME1=$_POST["name1"]; 
            $NAME2=$_POST["name2"];
			            	$subject='Registration Confirmation From National Level Technical Symposium XMPLARZ';
							$message="\n"."HI, ".$NAME1 ."&".$NAME2."\n \t Congratulations. ! you have registered For National Level Technical Symposium XMPLARZ";
							$headers="From: miteducenter@gmail.com";
						     mail ($to,$subject,$message,$headers);
							

        echo '<script>swal("Good job!", "You are Registered for a National Level Technical Symposium XMPLARZ .! we will notify you asap ! Thank You", "success");</script>';
    //   echo "posted";  
    } 
    else{
        echo '<script>swal("Error !", "There is some problem..! Try again later ! if continues please contact the coordinator! please check your abstract don\'t have the single quation \' please remove and try again", "error");</script>';
        // echo $sql;
    } 
}
}
?>

</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax").parallax();
        $(".tooltipped").tooltip();
        $('.sidenav').sidenav();
        $('.carousel').carousel();
        $('select').formSelect();
        $('#abstract').characterCounter();

        $('input.autocompletecollege').autocomplete({
            data: {
                "AVS Engineering College": null,
                "Annapoorana Engineering College": null,
                "Sona College of Technology": null,
                "Vinayaka Missions Kirupananda Variyar Engineering College": null,
                "VSA Engineering College": null,
                "Government College of Engineering, Salem": null,
                "Knowledge Institute of Technology": null,
                "Mahendra College of Engineering": null,
                "Mahendra Engineering College": null,
                "Shri Sakthikailassh Womens College": null,
                "Salem College of Engineering and Technology": null,
                "Narasus Sarathy Institute of Technology": null,
                "Sri Shanmugha College of Engineering, Sankari, Salem": null,
                "K.S.R Educational Institutions": null,
                "Mahendra Institute of Technology": null,

            },
        });
        $("#eventbtn").click(function() {
            var a = $("#event").val();
            alert(a);
        });


    });
</script>

</html>