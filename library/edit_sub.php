<?php
include ("../db.php");
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
    if(!isset($_GET["subid"]))
	{
		header('Location:admin_add_subject.php?mes=select subject first');
    }
    else {
        $sub_id=$_GET["subid"];
        $sql="SELECT * FROM subject WHERE SUB_ID=$sub_id;";
        $res=$db->query($sql);
        if($res->num_rows>0)
        {
            while($row=$res->fetch_assoc())
            {
                $sub_name=$row["SUB_NAME"];
            }
        }
    }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("lib_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="blur-box box">
                <form autocomplete="off" method="post" action="">
                    <legend>Add Subject name</legend>
                    <input type="text" name="sub_name" id="" value="<?php echo $sub_name; ?>" class="form-control" required>
                    <button type="submit" class="btn btn-primary" name="submit">Edit</button>
                </form>
                <?php
                if (isset($_POST["submit"])) {
                   $sub=$_POST["sub_name"];
                        $sql="UPDATE subject SET SUB_NAME='$sub' WHERE subject.SUB_ID=$sub_id;";
                        // echo $sql;
                       if($db->query($sql))
				        {
		                    header('Location:admin_add_subject.php?mes=Edited Successfully');
                        }
                }
                ?>
<br>
<br>
<br>
                <div class="blur-box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>S.No</th>
                                <th>Sub Name</th>
                                
                            </thead>
                            <tbody>
                                <?php
                                    $sql="SELECT * FROM subject";
                                    $res=$db->query($sql);
                                    if($res->num_rows>0)
                                    {
                                        $i=1;
                                        while($row=$res->fetch_assoc())
                                        {
                                            $sub_name=$row["SUB_NAME"];
                                            $sub_id=$row["SUB_ID"];
                                            echo "<tr>
                                            <td>$i</td>
                                            <td>$sub_name</td>
                                            </tr>";
                                            $i++;
                                        }
                                    }       
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
   

</body>


</html>