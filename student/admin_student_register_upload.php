<?php
	include"../db.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg3" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("stu_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


        <div class="container">

     <div class="row">
     <div class="col-sm-2"></div>
     <div class="col-sm-8">
     <div class="blur-box box">
     
   
     <div class=""> <h4>Register</h4> <a href="index.php" class="btn btn-success pull-right">Register</a></div>
     <br>
     <hr>
     <br>
     <form action="" method="post" autocomplete="off" enctype="multipart/form-data">
    <input type="file" name="myFile" class="form-control">
   <input type="submit" class="btn btn-info" name="uploadBtn">
    
</form>
      
<?php
if(isset($_POST['uploadBtn'])){
     $success=0;
     
    $fileName=$_FILES['myFile']['name'];
    $fileTmpName=$_FILES['myFile']['tmp_name'];
    // file extension
    $fileExtension=pathinfo($fileName,PATHINFO_EXTENSION);
    $allowedType=array('csv');
    if(!in_array($fileExtension,$allowedType)){
        echo"<div class='alert alert-danger'>
        invalid file extension</div>";
    }
    else{
        $handle=fopen($fileTmpName,'r');
        while(($myData=fgetcsv($handle,1000,','))!==FALSE){
           

      $name=$myData[0];
      $rollno=$myData[1];
      $regno=$myData[2];
      $dob=$myData[3];
      $blood=$myData[4];
      $contactno=$myData[5];
      $mailid=$myData[6];
      $parentno=$myData[7];
      $gender=$myData[8];
      $dept=$myData[9];
      $sec=$myData[10];
      $doj=$myData[11];
      $streetname=$myData[12];
      $place=$myData[13];
      $taluk=$myData[14];
      $doorno=$myData[15];
      $district=$myData[16];
      $pincode=$myData[17];
      $community=$myData[18];
      $religion=$myData[19];
      $nationality=$myData[20];
      $caste=$myData[21];
      $aadharno=$myData[22];
      $panno=$myData[23];
      $hscmark=$myData[24];
      $hscschool=$myData[25];
      $sslcmark=$myData[26];
      $sslcschool=$myData[27];
      $diplomamark=$myData[28];
      $diplomacollege=$myData[29];
      $admissiontype=$myData[30];
      $quota=$myData[31];
      $board=$myData[32];
      $batch=$myData[33];
      $semyear=$myData[34];
      $medium=$myData[35];
      $fathername=$myData[36];
      $fatheroccupation=$myData[37];
      $mothername=$myData[38];
      $motheroccupation=$myData[39];
      $income=$myData[40];
      $mothertongue=$myData[41];
      $firstgraduate=$myData[42];
      $classadvisor=$myData[43];
      $mentor=$myData[44];
 $sql="SELECT * FROM studentprofile WHERE REGNO='$regno';";
               $res=$db->query($sql);
		// echo $res->num_rows;
			if($res->num_rows>0)
			 {
                 echo"<div  class='alert alert-danger'>'$regno' already  available </div>";
				
			 }
			else{

     $sql ="INSERT INTO studentprofile ( REGNO, ROLLNO, NAME, DOB, BLOODGROUP, CONTACTNO, MAILID, PARENTCONTACTNO, GENDER, DEPARTMENT, SECTION, DOJ, SEMYEAR, FATHERNAME, FATHEROCCUPATION, MOTHERNAME, MOTHEROCCUPATION, ANNUALINCOME, MOTHERTONGUE, MEDIUM, BOARD, BATCH, ADMISSIONTYPE, QUOTA, PANNO, AADHARNO, HSCMARK, HSCSCHOOL, SSLCMARK, SSLCSCHOOL, DIPLOMAMARK, DIPLOMACOLLEGE, RELIGION, NATIONALITY, COMMUNITY, CASTE, DOORNO, STREETNAME, PLACE, TALUK, DISTRICT, PINCODE,FIRSTGRADUATE,CLASSADVISOR,MENTOR) VALUES ('$regno','$rollno','$name','$dob','$blood','$contactno','$mailid','$parentno','$gender','$dept','$sec','$doj','$semyear','$fathername','$fatheroccupation','$mothername','$motheroccupation','$income','$mothertongue','$medium','$board','$batch','$admissiontype','$quota','$panno','$aadharno','$hscmark','$hscschool','$sslcmark','$sslcschool','$diplomamark','$diplomacollege','$religion','$nationality','$community','$caste','$doorno','$streetname','$place','$taluk','$district','$pincode','$firstgraduate','$classadvisor','$mentor')";

            
           $res=$db->query($sql);
        
        if(!$res){
            die("error uploading file".mysql_error());
        }
        else{
           
            $success++;
        }
    }
    }
    echo"<div  class='alert alert-success'>'$success' datas uploaded succesfully</div>";
}
}
?>
     </div>
     </div>
     <div class="col-sm-2"></div>
     </div>
      </div>

</body>


</html>