<?php 	
include "db.php";
session_start();
	if(!isset($_SESSION["STUID"]))
	{
		header('Location:index.php?mes=please login as student');
    }
    else {
        $stuid=$_SESSION["STUID"];
    }
    ?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg5">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">
                    <?php
$stuid=$_SESSION["STUID"];
$sql="SELECT * FROM studentprofile WHERE STUID=$stuid;";
$res=$db->query($sql);
if($res->num_rows>0)
 {
 while($row=$res->fetch_assoc())
 {
   $name=$row["NAME"];
      $rollno=$row["ROLLNO"];
      $regno=$row["REGNO"];
      $dob=$row["DOB"];
 }
}
?>
                        <li> <a href="student_home.php" class="active "><span style="color:#fff; ">Welcome </span> <?php echo $name; ?></a></li>
<?php include("student_nav.php "); ?>
                       

                    </ul>


                </div>
            </div>

        </nav>
        <div class="container">

            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="blur-box box">
                        <div class="text-center">
                            <h4>Praveenram Balachandran</h4>
                            <h5>(2015 - 2019)</h5>
                            <br>
                            <h4>Aswath S</h4>
                            <h5>(2016 - 2020)</h5>
                            <br>
                            <h4> Sanjai Bal D</h4>
                            <h5>(2016 - 2020)</h5>

                            <br>
                            <h4> Nandhakumar T</h4>
                            <h5>(2016 - 2020)</h5>

                            <br>
                            <h2> Computer Science and Engineering</h2>
                        </div>
                    </div>
                    <br>
                    <div class="blur-box">
                        <div class="row blur-box">
                            <h5>Guided By</h5>
                            <div class="col-sm-6">
                                <div class="blur-box">
                                    <h4>Dr. J. Stanly Jayaprakash</h4>
                                    <h6>HOD - CSE</h6>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blur-box">
                                    <h4>Dr. V. Priya</h4>
                                    <h6>Professor - CSE</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-2"></div>
            </div>

        </div>



    </div>



</body>


</html>