<?php 	include"../db.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
    ?>

         <?php
                   if (!isset($_GET["stuid"])) {
                     echo"<script>window.open('admin_view_feedback_by_student.php','_self')</script>";
                   } 
                   else{
                       $stuid=$_GET["stuid"];
                   }
                   if (!isset($_GET["eventid"])) {
                     echo"<script>window.open('admin_view_feedback_by_student.php','_self')</script>";
                   }
                    else{
                       $eventid=$_GET["eventid"];
                   }
                   ?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg3" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("fb_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


        <div class="container">

<div class="row">
<div class="col-sm-2"></div>    
<div class="col-sm-8">
<div class="blur-box box">
<?php
$sql="SELECT * FROM eventdetail WHERE EVID=$eventid";
$res=$db->query($sql);
if($res->num_rows>0)
{
while($row=$res->fetch_assoc())
{
$evid=$row["EVID"];
$evname=$row["EV_NAME"];
$evdate=$row["EV_DATE"];
$evdescription=$row["EV_DESCRIPTION"];
$evtime=$row["EV_TIME"];
$evorganizer=$row["EV_ORGANIZER"];
$evresourceperson=$row["EV_RESOURCEPERSON"];
$evcoordinator=$row["EV_COORDINATOR"];
$evclass=$row["EV_CLASS"];
$evvenue=$row["EV_VENUE"];
}
echo"<div class='text-center'><h1 class='view-event-title text-uppercase'>$evname</h1></div>";
echo"<div class='row'>
<div class='col-sm-4'><div class='pull-left'><p><b>Resource person:</b> $evresourceperson<br> <b>Coordinator:</b> $evcoordinator<br><b>Organizer:</b> $evorganizer</p></div></div>
<div class='col-sm-4'></div>
<div class='col-sm-4'><div class='pull-right'><p><b>Date: </b>$evdate<br> <b>Time: </b>$evtime<br><b>Venue:</b> $evvenue<br><b>Class:</b> $evclass</p></div></div>
</div>";
echo"
<h4 class='view-desc-heading'>Description : </h4>
<p class='view-desc'>$evdescription</p>";
}
?>
</div>
<br>

<div class='blur-box'>
<div class="blur-box">
    <?php
$sql="SELECT * FROM studentprofile WHERE STUID=$stuid";
$res=$db->query($sql);
if($res->num_rows>0)
{
while($row=$res->fetch_assoc())
{
$NAME=$row["NAME"];
$REGNO=$row["REGNO"];
$DOB=$row["DOB"];
$CONTACTNO=$row["CONTACTNO"];
$MAILID=$row["MAILID"];
$DEPARTMENT=$row["DEPARTMENT"];
$SEMYEAR=$row["SEMYEAR"];
$SECTION=$row["SECTION"];

echo"<div class='row'>
<h4 class='text-center text-uppercase'>$NAME ($REGNO) </h4>
<div class='col-sm-6 pull-left'>
<div class='blur-box'>
<p>  <b>DEPARTMENT : </b> $DEPARTMENT</p>
<p>  <b>SEM / YEAR : </b> $SEMYEAR</p>
<p>  <b>SECTION : </b> $SECTION</p>
</div>
</div>
<div class='col-sm-6 pull-left'>
<div class='blur-box'>
<p>  <b>DOB : </b> $DOB</p>
<p>  <b>CONTACT NO : </b> $CONTACTNO</p>
<p>  <b>MAIL  : </b> $MAILID</p>
</div>
</div>
</div>";


echo"<br><div class='blur-box'>";
$sql="SELECT * FROM fbans WHERE FB_EVENTID=$evid AND FB_STUID=$stuid";
                                    $res=$db->query($sql);
                                    if($res->num_rows>0)
                                      {
                                          $total=0;
                                           $five=0;
                                          $four=0;
                                          $three=0;
                                          $two=0;
                                          $one=0;
                                     while($row=$res->fetch_assoc())
                                      {
                                          $fbeventid=$row["FB_EVENTID"];
                                          
                                          $fbans=$row["FB_ANS"];
                                         
                                          switch ($fbans) {
                                              case '5':
                                                  $five++;
                                                  $total++;
                                                  break;
                                              case '4':
                                                  $four++;
                                                  $total++;

                                                  break;
                                              case '3':
                                                  $three++;
                                                  $total++;

                                                  break;
                                              case '2':
                                                  $two++;
                                                  $total++;

                                                  break;
                                              case '1':
                                                  $one++;
                                                  $total++;

                                                  break;
                                              
                                              default:
                                                  # code...
                                                  break;
                                          }
                                      }




                                   
                                    
$five=$five/$total*100;
$four=$four/$total*100;
$three=$three/$total*100;
$two=$two/$total*100;
$one=$one/$total*100;
                                    echo"<div class='blur-box'>
                                   <h4>Feedback chart </h4>
<div class='progress'>
  <div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar'
  aria-valuenow='$five' aria-valuemin='0' aria-valuemax='100' style='width:$five%'>
    $five% EXCELLENT 
  </div>
</div>
<div class='progress'>
  <div class='progress-bar progress-bar-primary progress-bar-striped active' role='progressbar'
  aria-valuenow='$four' aria-valuemin='0' aria-valuemax='100' style='width:$four%'>
    $four% GOOD  
  </div>
</div>
<div class='progress'>
  <div class='progress-bar progress-bar-info progress-bar-striped active' role='progressbar'
  aria-valuenow='$three' aria-valuemin='0' aria-valuemax='100' style='width:$three%'>
    $three% BETTER 
  </div>
</div>
<div class='progress'>
  <div class='progress-bar progress-bar-warning progress-bar-striped active' role='progressbar'
  aria-valuenow='$two' aria-valuemin='0' aria-valuemax='100' style='width:$two%'>
    $two% AVERAGE 
  </div>
</div>
<div class='progress'>
  <div class='progress-bar progress-bar-danger progress-bar-striped active' role='progressbar'
  aria-valuenow='$one' aria-valuemin='0' aria-valuemax='100' style='width:$one%'>
    $one% UNSATISFACTORY 
  </div>
</div></div>";
}
echo"</div>";
}
}
?>
</div>
</div>
</div>    
<div class="col-sm-2"></div>    
</div>



            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
              


                   <?php
echo '<br><div class="blur-box">';
$sql="SELECT  eventquestions.QUESTION,fbans.FB_ANS FROM eventquestions INNER JOIN fbans ON eventquestions.QUID=fbans.FB_QUID WHERE fbans.FB_STUID=$stuid and fbans.FB_EVENTID=$eventid";
// echo" <br><br><br> $sql<br>";
 $res=$db->query($sql);
if($res->num_rows>0)
{
    $i=1;
    echo '<ul class="list-group">';
while($row=$res->fetch_assoc())
{
    $question=$row["QUESTION"];
    $ans=$row["FB_ANS"];
    switch ($ans) {
        case '5':
           $ans="Excellent";
            break;
        case '4':
           $ans="Good";
            break;
        case '3':
           $ans="Better";
            break;
        case '2':
           $ans="Average";
            break;
        case '1':
           $ans="Unsatisfactory";
            break;
        
    }
    echo" <li class='list-group-item'>$i) $question <span class='badge'>$ans</span></li>"; 

    $i++; 
}
}
else{
    echo"<h4>No record Found</h4>";
}

echo '</ul>';
$nextstuid=$stuid+1;
$prevstuid=$stuid-1;
echo"<a href='admin_view_by_student.php?eventid=$eventid&stuid=$prevstuid' class='btn btn-primary pull-left'><span class='fa fa-arrow-left'></span></a>";
echo"<a href='admin_view_by_student.php?eventid=$eventid&stuid=$nextstuid' class='btn btn-primary pull-right'><span class='fa fa-arrow-right'></span></a><br>";

echo '</div>';

?>

                    </div>
                    <div class="col-sm-2"></div>
                </div>

            </div>
            





        </div>



</body>


</html>