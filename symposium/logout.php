<?php
	include "../db.php";
	session_start();
	unset ($_SESSION["ADMIN_ID"]);
	unset ($_SESSION["ADMIN_USERNAME"]);
	session_destroy();
	echo "<script>window.open('admin_login.php','_self')</script>";
?>