<?php 	include"../db.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
    ?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg3" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("fb_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


        <div class="container">

            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="box">
                        <div class="blurbox">
                            <form action="" method="post" autocomplete="off">
                                <div class="form-group">
                                    <label for="" class="reglabel">Event Name</label>
                                    <select name="event_name" class="form-control">
                                    <?php
                                    $sql="SELECT * FROM eventdetail ORDER BY EVID DESC";
                                    $res=$db->query($sql);
                                    if($res->num_rows>0)
                                      {
                                     while($row=$res->fetch_assoc())
                                      {
                                          $eventname=$row["EV_NAME"];
                                        echo"<option  value='$eventname'><i class='text-uppercase'>$eventname</i></option>";
                                       
                                      }
                                    }
                                    ?>
                                    
                                       
                                    </select>
                                <input type="submit" name="viewevent" value="VIEW" class="btn btn-primary">
                                    
                                </div>
                        </div>

                    </div>
                    <div class="col-sm-2"></div>
                </div>

            </div>
             <div class="row">
<div class="col-sm-2"></div>
<div class="col-sm-8">
    <?php
if(isset($_POST["viewevent"])){
    $vieweventname=$_POST["event_name"];

    $sql="SELECT eventquestions.QUID, eventdetail.EVID,eventdetail.EV_NAME, eventdetail.EV_DATE, eventdetail.EV_DESCRIPTION,eventdetail.EV_TIME,eventdetail.EV_ORGANIZER,eventdetail.EV_RESOURCEPERSON,eventdetail.EV_COORDINATOR,eventdetail.EV_CLASS,eventdetail.EV_VENUE,eventquestions.QUESTION FROM eventdetail INNER JOIN eventquestions ON eventdetail.EVID=eventquestions.EVENT_ID WHERE eventdetail.EV_NAME='$vieweventname'";
 $res=$db->query($sql);
                                         $i=1;
$sl=1;
                                    if($res->num_rows>0)
                                      {
                                     while($row=$res->fetch_assoc())
                                      {
                                          $qid=$row["QUID"];
                                          $evid=$row["EVID"];
                                          $evname=$row["EV_NAME"];
                                          $evdate=$row["EV_DATE"];
                                          $evdescription=$row["EV_DESCRIPTION"];
                                          $evtime=$row["EV_TIME"];
                                          $evorganizer=$row["EV_ORGANIZER"];
                                          $evresourceperson=$row["EV_RESOURCEPERSON"];
                                          $evcoordinator=$row["EV_COORDINATOR"];
                                          $evclass=$row["EV_CLASS"];
                                          $evvenue=$row["EV_VENUE"];
                                          $evquestion=$row["QUESTION"];
                                         if($i==1){
                                             echo"<div class='text-center'><h1 class='view-event-title text-uppercase'>$evname</h1></div>";
                                             echo"<div class='row'>
                                             <div class='col-sm-4'><div class='pull-left'><p><b>Resource person:</b> $evresourceperson<br> <b>Coordinator:</b> $evcoordinator<br><b>Organizer:</b> $evorganizer</p></div></div>
                                             <div class='col-sm-4'></div>
                                             <div class='col-sm-4'><div class='pull-right'><p><b>Date: </b>$evdate<br> <b>Time: </b>$evtime<br><b>Venue:</b> $evvenue<br><b>Class:</b> $evclass</p></div></div>
                                             </div>";
                                             echo"
                                             <p class='pull-right'><a href='admin_edit_event.php?eventid=$evid' class='btn btn-edit'><span class='fa fa-edit'></span></a><a href='admin_delete_event.php?eventid=$evid' class='btn btn-delete'><span class='fa fa-trash'></span></a></p>
                                             <h4 class='view-desc-heading'>Description : </h4>
                                             <p class='view-desc'>$evdescription</p>
                                            ";
                                             
                                             echo"<h4 class='view-desc-heading'>Question : </h4>";
                                             $i++;
                                         }
                                             echo"<div class='card'>
                        <div class='view-event-qs'>$sl ) $evquestion</div>
                        <div class='view-event-btns pull-right'>
                            <a href='admin_edit_question.php?qid=$qid' class='btn btn-edit'><span class='fa fa-edit'></span></a>
                            <a href='admin_delete_question.php?qid=$qid' class='btn btn-delete'><span class='fa fa-trash'></span></a>
                        </div>
                        <br>
                    </div> <br><br>";

                                          $sl++;
                                      }
                                    }
}
    ?>
</div>
<div class="col-sm-2"></div>
            </div>






        </div>



</body>


</html>