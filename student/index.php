<?php
	include"../db.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg3" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("stu_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


        <div class="container">

     <div class="row">
     <div class="col-sm-2"></div>
     <div class="col-sm-8">
     <div class="blur-box box">
     
   
     <div class=""> <h4>Register</h4> <a href="admin_student_register_upload.php" class="btn btn-success pull-right">Upload CSV</a></div>
     <br>
     <hr>
     <br>
     <form class="form" style="max-height:500px; overflow:auto;" autocomplete="off" method="post" autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']?>">
<label>NAME:</label><input type="text" name="name" class="form-control" required><br>
<label>ROLLNO:</label><input type="text" name="rollno" class="form-control" required><br>
<label>REGNO:</label><input type="text" name="regno" class="form-control" required><br>
<label>DOB:</label><input type="date" name="dob" class="form-control" required><br>
<label>BLOOD GROUP:</label><input type="text" name="blood" class="form-control" required><br>
<label>Contact No</label><input type="text" name="contactno" class="form-control" ><br>
<label>MAILID</label><input type="email" name="mailid" class="form-control" required><br>
<label>PARENTNO</label><input type="text" name="parentno" class="form-control" required><br>
<label>Gender</label><select  name="gender" class="form-control" required>
    <option value="MALE">MALE</option>
    <option value="FEMALE">FEMALE</option>
    <option value="OTHERS">OTHER</option>
</select>
<br>
<label>Department</label><select  name="dept" class="form-control" required>
    <option value="Computer Science and Engineering">CSE</option>  
</select>
<br>

<label>SECTION</label><select  name="sec" class="form-control" required>
    <option value="A">A</option>  
    <option value="B">B</option>  
    <option value="C">C</option>  
    <option value="D">D</option>  
</select><br>
<label>DATE OF JOINING:</label><input type="date" name="doj" class="form-control" required><br>
<label>Doorno</label><input type="text" name="doorno" class="form-control" required><br>
<label>streetname</label><input type="text" name="streetname" class="form-control" required><br>
<label>place</label><input type="text" name="place" class="form-control" required><br>
<label>taluk</label><input type="text" name="taluk" class="form-control" required><br>
<label>district</label><input type="text" name="district" class="form-control" required><br>
<label>pincode</label><input type="number" name="pincode" class="form-control" required><br>
<label>community</label><input type="text" name="community" class="form-control" required><br>
<label>nationality</label><input type="text" name="nationality" class="form-control" required><br>
<label>religion</label><input type="text" name="religion" class="form-control" required><br>
<label>caste</label><input type="text" name="caste" class="form-control" required><br>
<label>aadhar no</label><input type="text" name="aadharno" class="form-control" required><br>
<label>pan no</label><input type="text" name="panno" class="form-control" required><br>
<label>hsc mark</label><input type="text" name="hscmark" class="form-control" ><br>
<label>hsc school</label><input type="text" name="hscschool" class="form-control" ><br>
<label>sslc  mark</label><input type="text" name="sslcmark" class="form-control" required><br>
<label>sslc school</label><input type="text" name="sslcschool" class="form-control" required><br>
<label>diploma mark</label><input type="text" name="diplomamark" class="form-control" ><br>
<label>diploma college</label><input type="text" name="diplomacollege" class="form-control" ><br>
<label>admission type</label><select  name="admissiontype" class="form-control" required>
    <option value="REGULAR">REGULAR</option>  
    <option value="LATERAL">LATERAL</option>  
   
</select><br>
<label>quota</label><select  name="quota" class="form-control" required>
    <option value="GQ">GOVERNMENT QUOTA</option>  
    <option value="MQ">MANAGEMENT QUOTA</option>  
   
</select><br>
<label>BOARD</label><select  name="board" class="form-control" required>
    <option value="STATE">STATE</option>  
    <option value="CBSE">CBSE</option>  
   
</select><br>
<label>BATCH</label><input type="text" name="batch" class="form-control" required><br>
<label>sem/year</label><input type="text" name="semyear" class="form-control" required><br>
<label>medium</label><input type="text" name="medium" class="form-control" required><br>
<label>fathername</label><input type="text" name="fathername" class="form-control" required><br>
<label>fatheroccupation</label><input type="text" name="fatheroccupation" class="form-control" required><br>
<label>mothername</label><input type="text" name="mothername" class="form-control" required><br>
<label>motheroccupation</label><input type="text" name="motheroccupation" class="form-control" required><br>
<label>annual income</label><input type="text" name="income" class="form-control" required><br>
<label>mothertongue</label><input type="text" name="mothertongue" class="form-control" required><br>
<label>Class advisor</label><input type="text" name="classadvisor" class="form-control" required><br>
<label>mentor</label><input type="text" name="mentor" class="form-control" required><br>
<label>firstgraduate</label><select  name="firstgraduate" class="form-control" required>
    <option value="YES">YES</option>  
    <option value="NO">NO</option>  
   
</select><br>
<input type="submit" class="btn btn-primary" name="submit" value="submit">
</form>
     </div>
     </div>
     <div class="col-sm-2"></div>
     </div>
      </div>

 <?php
     if (isset($_POST["submit"])) {
    //   echo  print_r($_POST);

      $name=$_POST["name"];
      $rollno=$_POST["rollno"];
      $regno=$_POST["regno"];
      $dob=$_POST["dob"];
      $blood=$_POST["blood"];
      $contactno=$_POST["contactno"];
      $mailid=$_POST["mailid"];
      $parentno=$_POST["parentno"];
      $gender=$_POST["gender"];
      $dept=$_POST["dept"];
      $sec=$_POST["sec"];
      $doj=$_POST["doj"];
      $streetname=$_POST["streetname"];
      $place=$_POST["place"];
      $taluk=$_POST["taluk"];
      $doorno=$_POST["doorno"];
      $district=$_POST["district"];
      $pincode=$_POST["pincode"];
      $community=$_POST["community"];
      $religion=$_POST["religion"];
      $nationality=$_POST["nationality"];
      $caste=$_POST["caste"];
      $aadharno=$_POST["aadharno"];
      $panno=$_POST["panno"];
      $hscmark=$_POST["hscmark"];
      $hscschool=$_POST["hscschool"];
      $sslcmark=$_POST["sslcmark"];
      $sslcschool=$_POST["sslcschool"];
      $diplomamark=$_POST["diplomamark"];
      $diplomacollege=$_POST["diplomacollege"];
      $admissiontype=$_POST["admissiontype"];
      $quota=$_POST["quota"];
      $board=$_POST["board"];
      $batch=$_POST["batch"];
      $semyear=$_POST["semyear"];
      $medium=$_POST["medium"];
      $fathername=$_POST["fathername"];
      $fatheroccupation=$_POST["fatheroccupation"];
      $mothername=$_POST["mothername"];
      $motheroccupation=$_POST["motheroccupation"];
      $income=$_POST["income"];
      $mothertongue=$_POST["mothertongue"];
      $firstgraduate=$_POST["firstgraduate"];
      $classadvisor=$_POST["classadvisor"];
      $mentor=$_POST["mentor"];


      $sql="SELECT * FROM studentprofile WHERE REGNO='$regno';";
               $res=$db->query($sql);
		// echo $res->num_rows;
			if($res->num_rows>0)
			 {
				echo '<script>alert("Sorry! The Registerno  '.$regno.'  already available");</script>';
			 }
			else{
$sql ="INSERT INTO studentprofile ( REGNO, ROLLNO, NAME, DOB, BLOODGROUP, CONTACTNO, MAILID, PARENTCONTACTNO, GENDER, DEPARTMENT, SECTION, DOJ, SEMYEAR, FATHERNAME, FATHEROCCUPATION, MOTHERNAME, MOTHEROCCUPATION, ANNUALINCOME, MOTHERTONGUE, MEDIUM, BOARD, BATCH, ADMISSIONTYPE, QUOTA, PANNO, AADHARNO, HSCMARK, HSCSCHOOL, SSLCMARK, SSLCSCHOOL, DIPLOMAMARK, DIPLOMACOLLEGE, RELIGION, NATIONALITY, COMMUNITY, CASTE, DOORNO, STREETNAME, PLACE, TALUK, DISTRICT, PINCODE,FIRSTGRADUATE,CLASSADVISOR,MENTOR) VALUES ('$regno','$rollno','$name','$dob','$blood','$contactno','$mailid','$parentno','$gender','$dept','$sec','$doj','$semyear','$fathername','$fatheroccupation','$mothername','$motheroccupation','$income','$mothertongue','$medium','$board','$batch','$admissiontype','$quota','$panno','$aadharno','$hscmark','$hscschool','$sslcmark','$sslcschool','$diplomamark','$diplomacollege','$religion','$nationality','$community','$caste','$doorno','$streetname','$place','$taluk','$district','$pincode','$firstgraduate','$classadvisor','$mentor')";
 if($db->query($sql))
				{
				echo '<script>alert("added successfully");</script>';
				}
				else
				{
				echo '<script>swal("error occured");</script>';
				
				}


            }

   }
   
   ?> 

</body>


</html>