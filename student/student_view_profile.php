<?php 	
include"../db.php";
session_start();
	if(!isset($_SESSION["STUID"]))
	{
		header('Location:../index.php?mes=please login as student');
    }
    else {
        $stuid=$_SESSION["STUID"];
    }
    ?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg4"">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">
<?php
$stuid=$_SESSION["STUID"];
$sql="SELECT * FROM studentprofile WHERE STUID=$stuid;";
$res=$db->query($sql);
if($res->num_rows>0)
 {
 while($row=$res->fetch_assoc())
 {
   $STUID=$row["STUID"];
   $REGNO=$row["REGNO"];
   $ROLLNO=$row["ROLLNO"];
   $NAME=$row["NAME"];
   $name=$row["NAME"];
   $DOB=$row["DOB"];
   $BLOODGROUP=$row["BLOODGROUP"];
   $CONTACTNO=$row["CONTACTNO"];
   $MAILID=$row["MAILID"];
   $PARENTCONTACTNO=$row["PARENTCONTACTNO"];
   $GENDER=$row["GENDER"];
   $DEPARTMENT=$row["DEPARTMENT"];
   $SECTION=$row["SECTION"];
   $DOJ=$row["DOJ"];
   $SEMYEAR=$row["SEMYEAR"];
   $FATHERNAME=$row["FATHERNAME"];
   $FATHEROCCUPATION=$row["FATHEROCCUPATION"];
   $MOTHERNAME=$row["MOTHERNAME"];
   $MOTHEROCCUPATION=$row["MOTHEROCCUPATION"];
   $ANNUALINCOME=$row["ANNUALINCOME"];
   $MOTHERTONGUE=$row["MOTHERTONGUE"];
   $MEDIUM=$row["MEDIUM"];
   $BOARD=$row["BOARD"];
   $BATCH=$row["BATCH"];
   $ADMISSIONTYPE=$row["ADMISSIONTYPE"];
   $QUOTA=$row["QUOTA"];
   $PANNO=$row["PANNO"];
   $AADHARNO=$row["AADHARNO"];
   $HSCMARK=$row["HSCMARK"];
   $HSCSCHOOL=$row["HSCSCHOOL"];
   $SSLCMARK=$row["SSLCMARK"];
   $SSLCSCHOOL=$row["SSLCSCHOOL"];
   $DIPLOMAMARK=$row["DIPLOMAMARK"];
   $DIPLOMACOLLEGE=$row["DIPLOMACOLLEGE"];
   $RELIGION=$row["RELIGION"];
   $NATIONALITY=$row["NATIONALITY"];
   $COMMUNITY=$row["COMMUNITY"];
   $CASTE=$row["CASTE"];
   $DOORNO=$row["DOORNO"];
   $STREETNAME=$row["STREETNAME"];
   $PLACE=$row["PLACE"];
   $TALUK=$row["TALUK"];
   $DISTRICT=$row["DISTRICT"];
   $PINCODE=$row["PINCODE"];
   $CLASSADVISOR=$row["CLASSADVISOR"];
   $MENTOR=$row["MENTOR"];
   $FIRSTGRADUATE=$row["FIRSTGRADUATE"];
   $PHOTO=$row["PHOTO"];
   $SIGNATURE=$row["SIGNATURE"];


 }
}
?>
                        <li> <a href="../student_home.php" class="active"><span style="color:#fff;">Welcome </span> <?php echo $name; ?></a></li>
<?php include("stu_student_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>
<div class="container">
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 blur-box box">
            <div class="">
            <div class="text-center"> <div class="btn btn-group" ><button id="profile_btn" class="btn btn-primary">Profile</button> <button  id="phonebook_btn" class="btn btn-success">Contact</button ><button  id="family_btn" class="btn btn-default">Family Details</button ><button id="informative_btn" class="btn btn-info">Informative Content</button><button id="mark_btn" class="btn btn-warning">Marks </button></div> </div>
            </div>

<div class="col-sm-1"></div>

</div></div>
</div>

 <div class="container">
     <br>
     <div class="row" id="profile">
         <div class="col-sm-1"></div>
         <div class="col-sm-10 blur-box2">
         <div class="col-sm-6">
                <p class="text-uppercase"><b >NAME : </b> <?php echo $NAME; ?></p>
                <p class="text-uppercase"><b >REGNO : </b> <?php echo $REGNO; ?></p>
                <p class="text-uppercase"><b >ROLLNO : </b> <?php echo $ROLLNO; ?></p>
                <p class="text-uppercase"><b >BATCH : </b> <?php echo $BATCH; ?></p>
                <p class="text-uppercase"><b >GENDER : </b> <?php echo $GENDER; ?></p>
                <p class="text-uppercase"><b >DOB : </b> <?php echo $DOB; ?></p>
                <p class="text-uppercase"><b >SEMYEAR : </b> <?php echo $SEMYEAR; ?></p>
                <p class="text-uppercase"><b >SECTION : </b> <?php echo $SECTION; ?></p>
                <p class="text-uppercase"><b>DEPARTMENT : </b> <?php echo $DEPARTMENT; ?></p>
              
         </div>
         <div class="col-sm-6">
             <div class="col-sm-3"></div>
             <div class="col-sm-6">
                 <img src="<?php echo $PHOTO ;?>" class="img img-thumbnail img-responsive img-circle">
             </div>
             <div class="col-sm-3"></div>
         </div>
         </div>
         <div class="col-sm-1"></div>
     </div>
        <div class="row" id="contact">
         <div class="col-sm-1"></div>
         <div class="col-sm-10 blur-box2">
         <div class="col-sm-6">
                <p class="text-uppercase"><b >CONTACTNO : </b> <?php echo $CONTACTNO; ?></p>
                <p class="text-uppercase"><b >PARENT CONTACT NO : </b> <?php echo $PARENTCONTACTNO; ?></p>
                <p class=""><b >MAILID : </b> <?php echo $MAILID; ?></p>
               
              
         </div>
         <div class="col-sm-6">
             <div class="col-sm-3"></div>
             <div class="col-sm-6">
                  <p class="text-uppercase"><b >ADDRESS : </b> <?php echo"<br> $DOORNO,$STREETNAME,<br>$PLACE,<br>$TALUK,<br>$DISTRICT,<br>$PINCODE";?></p>
                
             </div>
             <div class="col-sm-3"></div>
         </div>
         </div>
         <div class="col-sm-1"></div>
     </div>



        <div class="row" id="family">
         <div class="col-sm-1"></div>
         <div class="col-sm-10 blur-box2">
         <div class="col-sm-6">
                <p class="text-uppercase"><b >FATHER NAME : </b> <?php echo $FATHERNAME; ?></p>
                <p class="text-uppercase"><b >FATHER OCCUPATION : </b> <?php echo $FATHEROCCUPATION; ?></p>
                
               
              
         </div>
         <div class="col-sm-6">
             <div class="col-sm-3"></div>
             <div class="col-sm-6">
                   <p class="text-uppercase"><b >MOTHER NAME : </b> <?php echo $MOTHERNAME; ?></p>
                <p class="text-uppercase"><b >MOTHER OCCUPATION : </b> <?php echo $MOTHEROCCUPATION; ?></p>
                
             </div>
             <div class="col-sm-3"></div>
         </div>
         </div>
         <div class="col-sm-1"></div>
     </div>


        <div class="row" id="information">
         <div class="col-sm-1"></div>
         <div class="col-sm-10 blur-box2">
         <div class="col-sm-6">
                <p class="text-uppercase"><b >BLOODGROUP : </b> <?php echo $BLOODGROUP; ?></p>
                <p class="text-uppercase"><b >DOJ : </b> <?php echo $DOJ; ?></p>
                <p class="text-uppercase"><b >MOTHERTONGUE : </b> <?php echo $MOTHERTONGUE; ?></p>
                <p class="text-uppercase"><b >BOARD : </b> <?php echo $BOARD; ?></p>
                <p class="text-uppercase"><b >MEDIUM : </b> <?php echo $MEDIUM; ?></p>
                <p class="text-uppercase"><b >ADMISSIONTYPE : </b> <?php echo $ADMISSIONTYPE; ?></p>
                <p class="text-uppercase"><b >QUOTA : </b> <?php echo $QUOTA; ?></p>
                <p class="text-uppercase"><b >PANNO : </b> <?php echo $PANNO; ?></p>
                <p class="text-uppercase"><b >AADHARNO : </b> <?php echo $AADHARNO; ?></p>
                
               
                
               
              
         </div>
         <div class="col-sm-6">
             <div class="col-sm-3"></div>
             <div class="col-sm-6">
              <p class="text-uppercase"><b >RELIGION : </b> <?php echo $RELIGION; ?></p>
                <p class="text-uppercase"><b >NATIONALITY : </b> <?php echo $NATIONALITY; ?></p>
                <p class="text-uppercase"><b >COMMUNITY : </b> <?php echo $COMMUNITY; ?></p>
                <p class="text-uppercase"><b >ANNUALINCOME : </b> <?php echo $ANNUALINCOME; ?></p>
                <p class="text-uppercase"><b >CASTE : </b> <?php echo $CASTE; ?></p>
                <p class="text-uppercase"><b >FIRST GRADUATE : </b> <?php echo $FIRSTGRADUATE; ?></p>
                <p class="text-uppercase"><b >CLASSADVISOR : </b> <?php echo $CLASSADVISOR; ?></p>
                <p class="text-uppercase"><b >MENTOR : </b> <?php echo $MENTOR; ?></p>
             </div>
             <div class="col-sm-3"></div>
         </div>
         </div>
         <div class="col-sm-1"></div>
     </div>



     <div class="row" id="marks">
         <div class="col-sm-1"></div>
         <div class="col-sm-10 blur-box2">
         <div class="table-responsive">
             <table class="table table-stripped">
                    <thead class="text-uppercase">
                        <th>Education</th>
                        <th>Mark</th>
                        <th>School</th>
                        <th>Percentage</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>SSLC</td>
                            <td><?php echo $SSLCMARK ; ?></td>
                            <td><?php echo $SSLCSCHOOL ; ?></td>
                            <td><?php $SSLCPERCENTAGE=$SSLCMARK/500*100;  echo"$SSLCPERCENTAGE %"  ; ?></td>
                            
                        </tr>
                        <tr>
                            <td>HSC</td>
                            <td><?php echo $HSCMARK ; ?></td>
                            <td><?php echo $HSCSCHOOL ; ?></td>
                            <td><?php $HSCPERCENTAGE=$HSCMARK/1200*100;  echo"$HSCPERCENTAGE %"  ; ?></td>
                            
                        </tr>
                        <tr>
                            <td>DIPLOMA</td>
                            <td><?php echo $DIPLOMAMARK ; ?></td>
                            <td><?php echo $DIPLOMACOLLEGE ; ?></td>
                            <td>-</td>
                            
                        </tr>
                    </tbody>
             </table>
         </div>


         </div>
         </div>
         <div class="col-sm-1"></div>
     </div>
   
     








     
 </div>



</body>
<script>
$("document").ready(function(){
    $("#family").hide();
    $("#information").hide();
    $("#marks").hide();
    $("#contact").hide();
    $("#profile_btn").click(function(){
         $("#family").hide();
    $("#information").hide();
    $("#marks").hide();
    $("#contact").hide();
    $("#profile").show();
    });    
    $("#family_btn").click(function(){
         $("#profile").hide();
    $("#information").hide();
    $("#marks").hide();
    $("#contact").hide();
    $("#family").show();
    });
    $("#phonebook_btn").click(function(){
         $("#profile").hide();
    $("#information").hide();
    $("#marks").hide();
    $("#family").hide();
    $("#contact").show();
    });
    $("#mark_btn").click(function(){
         $("#profile").hide();
    $("#information").hide();
    $("#contact").hide();
    $("#family").hide();
    $("#marks").show();
    });
    $("#informative_btn").click(function(){
         $("#profile").hide();
    $("#marks").hide();
    $("#contact").hide();
    $("#family").hide();
    $("#information").show();
    });
});
</script>

</html>