<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>
 
            <nav id="nav" class="text-uppercase">
                <div class="nav-wrapper">
                    <a href="#!" class="brand-logo">XMPLARZ</a>
                    <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="home.php">home</a></li>
                        <li><a href="register.php">register</a></li>
                        <li class="active"><a href="aboutus.php">aboutus</a></li>
                    </ul>
                </div>
            </nav>

            <ul class="sidenav text-uppercase" id="mobile-demo">
                <li><a href="home.php">home</a></li>
                <li><a href="register.php">register</a></li>
                <li class="active"><a href="aboutus.php">aboutus</a></li>
            </ul>
      
    <div class="parallax-container">
       
        <div class="parallax"><img src="img/banner2.jpg">

        </div>
    </div>

    <div class="row section  ">
        <div class="container ">
            <h3 class="primary_heading wow bounceInLeft"><a class=" tooltipped" data-position="bottom" data-tooltip="Mahendra Insitute of Technology">Mahendra Institute of Technology </a></h3>
            <p class="text-justify paragraph-indent primary_paragraph wow bounceInRight">Mahendra Institute of Technology was established in 2007 by the Mahendra Educational Trust under the visionary leadership of renowned educationalist <strong>Shri. M.G. Bharath Kumar</strong>, is the Chairman of the Trust and a well-known Philanthropist
                in this region. The College is situated on the Salem-to-Tiruchengode main road, about 25 km away from Salem. Mahendra Institute of Technology is approved by AICTE and affiliated to Anna University - Chennai. Currently, it offers eight
                undergraduate programmes and three postgraduate programmes in Engineering & Technology. The college has research centre’s in Mechanical Engineering and Electronics and Communication Engineering through which doctorate programmes are offered.
            </p>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="img/contemporary-desk-eyeglasses-1006293.jpg"></div>
    </div>
    <div class="row section  ">
        <div class="container ">
            <h3 class="primary_heading wow jackInTheBox"><a class="tooltipped" data-position="bottom" data-tooltip="Computer Science and Engineering">Computer
Science and Engineering </a></h3>
            <p class="text-justify paragraph-indent primary_paragraph wow fadeInLeft">The Computer Science and Engineering department imparts quality education along with bestowing our students with a stimulating and enabling learning environment. The department boasts of eclectic blend of Dedicated and learned faculty members,
                top-notch learning resources and robust teaching practices crucial for meeting the challenges of today’s cut throat competitive world. The academics are further buttressed by slew of programs and events designed to broad base students
                learning within and outside of the cozy confines of the class rooms. The department is committed to investing in developing the all-round personality of the students and prepares our students to make a name in the sun. We, at CSE foster
                innovation and creativity by recalibrating and re-orienting our programs and events though strait- jacketed by our limited freedom in setting the curriculum and syllabus. The emphasis we place on Value Added courses, placement and training
                in various software platforms holds the key to our student’s successful placements records every year. Aside from the academics, the heavy bias on promoting social and ethical values is the bedrock of our department helping our students
                contributes their mite towards national building.</p>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="img/alphabet-class-conceptual-301926.jpg"></div>
    </div>
    <div class="row section">
        <div class="container">
            <h3 class="primary_heading wow rollIn"><a class="tooltipped" data-position="bottom" data-tooltip="Pillers of CSE">Faculties</a></h3>

            <div class="carousel  text-center">
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#one!"><img src="http://xmplarz.000webhostapp.com/images/team/principal.jpg"><span class="faculty_name text-center"><b> Dr. T. Elango M.E., Ph.D., M.I.E.,</b><br><i>PRINCIPAL</i></span></a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/hod.jpg"><span class="faculty_name"><b> Dr. J. Stanly Jaya Prakash M.E.,Ph.D., </b><br> <i>HOD-CSE</i></span></a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/latha.jpg"><span class="faculty_name"><b> Mrs. S. Latha M.E., </b><br> <i>AP/CSE</i></span></a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/karthikeyan.jpg"><span class="faculty_name"><b>Mr. A. N. Karthikeyan M.E., </b> <br> <i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/anbumani.jpg"><span class="faculty_name"><b> Mr. A. Anbumani M.E., </b><br><i>AP/CSE</i></span></a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/gayathri.jpg"><span class="faculty_name"><b> Mrs. C. Gayathri M.E., </b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/savitha.jpg"><span class="faculty_name"><b>Mrs. K. Savitha M.E., </b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/mohanrajsir.jpg"><span class="faculty_name"><b>Mr. B. Mohanraj M.E.,</b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/saranya.jpg"><span class="faculty_name"><b>Ms. R. Saranya M.E.,</b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/kalai.jpg"><span class="faculty_name"><b>Mrs. V. Kalaiyarasi M.E.,</b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/priyanka.jpg"><span class="faculty_name"><b>Ms. S. Priyanga M.E., M.E.,</b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/nisha.jpg"><span class="faculty_name"><b>Mrs. U. Nilabar Nisha M.E.,</b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/logesh.jpg"><span class="faculty_name"><b>Mr. R. Logesh Babu M.E.,</b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/sowmiya.jpg"><span class="faculty_name"><b>Ms. R. Sowmiya M.E.,</b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/krishnan.jpg"><span class="faculty_name"><b>Mr. C. Krishnan M.E.,</b><br><i>AP/CSE</i></span> </a>
                <a class="carousel-item wow fadeIn" data-wow-duration="2s" href="#two!"><img src="http://xmplarz.000webhostapp.com/images/team/abdul.jpg"><span class="faculty_name"><b>Mr. D. Abdul Jaleel M.E.,</b><br><i>AP/CSE</i></span> </a>
            </div>


        </div>
    </div>


    <div class="parallax-container">
        <div class="parallax"><img src="img/art-art-materials-artistic-208144.jpg"></div>
    </div>
    <div class="row section">
        <div class="container">
            <h3 class="primary_heading wow flip"><a class="tooltipped" data-position="bottom" data-tooltip="Organizers">Team</a></h3>
            <h5 class="secondary_heading">Overall coordinators</h5>
            <div class="row">
                <div class="col s12 m4">
                    <div class="card wow slideInUp">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="img/2.jpg">
                        </div>
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">Praveenram Balachandran<i class="material-icons right">more_vert</i></span>
                            <p>Final Year CSE</p>
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">Praveenram Balachandran<i class="material-icons right">close</i></span>
                            <p>Praveenram Balachandran Full Stack WEB DEVELOPER, Overall Coordinator of the whole event</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="card wow slideInUp">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="img/2.jpg">
                        </div>
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">Praveenram Balachandran<i class="material-icons right">more_vert</i></span>
                            <p>Final Year CSE</p>
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">Praveenram Balachandran<i class="material-icons right">close</i></span>
                            <p>Praveenram Balachandran Full Stack WEB DEVELOPER, Overall Coordinator of the whole event</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="card wow slideInUp">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="img/2.jpg">
                        </div>
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">Praveenram Balachandran<i class="material-icons right">more_vert</i></span>
                            <p>Final Year CSE</p>
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">Praveenram Balachandran<i class="material-icons right">close</i></span>
                            <p>Praveenram Balachandran Full Stack WEB DEVELOPER, Overall Coordinator of the whole event</p>
                        </div>
                    </div>
                </div>

            </div>

            <!-- next row -->



        </div>
    </div>

</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax").parallax();
        $(".tooltipped").tooltip();
        $('.sidenav').sidenav();
        $('.carousel').carousel();
    });
</script>

</html>