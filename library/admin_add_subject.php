<?php
include ("../db.php");
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("lib_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="blur-box box">
              
                <form autocomplete="off" method="post" action="">
                    <legend>Add Subject name</legend>
                    <input type="text" name="sub_name" id="" class="form-control" required>
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                </form>
<br>
                  <?php
                if (isset($_GET["mes"])) 
                {
                    $war=$_GET["mes"];
                    echo "<div class='alert alert-danger alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>$war</strong>
                        </div>";
                }
                ?>

                <?php
                if (isset($_POST["submit"])) {
                   $sub=$_POST["sub_name"];
                   $sql="SELECT * FROM subject WHERE SUB_NAME='$sub';";
                //    echo $sql;
                   $res=$db->query($sql);
                    if($res->num_rows>0)
                    {
                        // echo "found";
                        echo "<script>alert('sorry ! This subject $sub is already available');</script>";
                    }
                    else{
                        $sql="INSERT INTO subject (SUB_NAME) VALUES ('$sub');";
                        // echo $sql;
                       if($db->query($sql))
				        {
                        echo "<script>alert('Good Job ! the subject added');</script>";
                            // echo "not found";
                        }
                    }
                }
                ?>
<br>
<br>
<br>
                <div class="blur-box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>S.No</th>
                                <th>Sub Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $sql="SELECT * FROM subject";
                                    $res=$db->query($sql);
                                    if($res->num_rows>0)
                                    {
                                        $i=1;
                                        while($row=$res->fetch_assoc())
                                        {
                                            $sub_name=$row["SUB_NAME"];
                                            $sub_id=$row["SUB_ID"];
                                            echo "<tr>
                                            <td>$i</td>
                                            <td>$sub_name</td>
                                            <td><a href='edit_sub.php?subid=$sub_id' class='btn btn-default'><span class='fa fa-edit'></span></a></td>
                                            <td><a href='delete_sub.php?subid=$sub_id' class='btn btn-danger'><span class='fa fa-trash'></span></a></td>
                                            </tr>";
                                            $i++;
                                        }
                                    }       
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
   

</body>


</html>