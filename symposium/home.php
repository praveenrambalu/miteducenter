<!DOCTYPE html>
<html>

<head>
 <?php include("stuffs.php"); ?>

</head>

<body>
    <div class="row fullpage ">
        <nav id="nav" class="text-uppercase">
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo">XMPLARZ</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li class="active"><a href="home.php">home</a></li>

                    <li><a href="register.php">register</a></li>
                    <li><a href="aboutus.php">aboutus</a></li>
                </ul>
            </div>
        </nav>

        <ul class="sidenav text-uppercase" id="mobile-demo">
            <li class="active"><a href="home.php">home</a></li>

            <li><a href="register.php">register</a></li>
            <li><a href="aboutus.php">aboutus</a></li>
        </ul>
        <div class="col s12 m6 background1 wow slideInLeft"></div>
        <div class="col s12 m6">
            <div class="vertialign-wrapper center tooltipped" data-position="bottom" data-tooltip="Press the Heart to Know more">
                <h1 class="events_primary_heading center  wow slideInRight">Prezentazy
                </h1>
                <p class="text-center text-justify wow slideInUp">Stage your Ideas by Paper Presentation</p>
                <a class="waves-effect waves-light wow  infinite pulse" href="paper_presentation.php"><i class="material-icons">favorite_border</i></a>
                <!-- <i class="material-icons cyan-text">favorite_border</i> -->
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>

    <div class="row fullpage">

        <div class="col s12 m6">
            <div class="vertialign-wrapper center tooltipped" data-position="bottom" data-tooltip="Press the Heart to Know more">
                <h1 class="events_primary_heading center wow fadeInLeft">Bug Hunting
                </h1>
                <p class="text-center text-justify wow slideInUp">Identify the Killer Bugs in the Program</p>
                <a class="waves-effect waves-light wow  infinite pulse" href="code_debugging.php"><i class="material-icons">favorite_border</i></a>
                <!-- <i class="material-icons cyan-text">favorite_border</i> -->
            </div>
        </div>
        <div class="col s12 m6 background2 wow fadeInRight"></div>
    </div>
    <div class="row fullpage">
        <div class="col s12 m6 background3 wow bounceInLeft"></div>

        <div class="col s12 m6">
            <div class="vertialign-wrapper center tooltipped" data-position="bottom" data-tooltip="Press the Heart to Know more">
                <h1 class="events_primary_heading center wow  slideInUp ">Kviz
                </h1>
                <p class="text-center text-justify wow fadeIn">Inky Pinky ponky the Correct answer ;-)</p>
                <a class="waves-effect waves-light   wow  infinite pulse" href="online_quiz.php"><i class="material-icons">favorite_border</i></a>
                <!-- <i class="material-icons cyan-text">favorite_border</i> -->
            </div>
        </div>
    </div>
    <div class="row fullpage">

        <div class="col s12 m6">
            <div class="vertialign-wrapper center tooltipped" data-position="bottom" data-tooltip="Press the Heart to Know more">
                <h1 class="events_primary_heading center wow fadeInLeft">Ad-Mad
                </h1>
                <p class="text-center text-justify wow slideInUp">A Crazy way to sell your Product. ;-)
                </p>
                <a class="waves-effect waves-light wow  infinite pulse" href="advertisement.php"><i class="material-icons">favorite_border</i></a>
                <!-- <i class="material-icons cyan-text">favorite_border</i> -->
            </div>
        </div>
        <div class="col s12 m6 background4 wow fadeInRight"></div>
    </div>
    <div class="row fullpage">
        <div class="col s12 m6 background5 wow bounceInLeft"></div>

        <div class="col s12 m6">
            <div class="vertialign-wrapper center tooltipped" data-position="bottom" data-tooltip="Press the Heart to Know more">
                <h1 class="events_primary_heading center wow  slideInUp ">Fun 'o' Mania
                </h1>
                <p class="text-center text-justify wow fadeIn">Let Join with us in game world</p>
                <a class="waves-effect waves-light   wow  infinite pulse" href="fun_games.php"><i class="material-icons">favorite_border</i></a>
                <!-- <i class="material-icons cyan-text">favorite_border</i> -->
            </div>
        </div>
    </div>
</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax").parallax();
        $(".tooltipped").tooltip();
        $('.sidenav').sidenav();
        $('.carousel').carousel();

    });
</script>

</html>