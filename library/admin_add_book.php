<?php
	include"../db.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }
    
   
?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>
</head>

<body>

    <div class="bgimg6" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("lib_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>

<?php
$admin_id=$_SESSION["ADID"];
$sql="SELECT * FROM facultyprofile WHERE FAID=$admin_id;";
 $res=$db->query($sql);
if($res->num_rows>0)
{
    while($row=$res->fetch_assoc())
    {
        $admin_name=$row["NAME"];
    }
}

?>
<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="blur-box box">
                <div class="pull-right">
                    <a href="admin_add_book_url.php" class="btn btn-primary"><span class="fa fa-globe"></span></a>
                </div>
                <?php
	if(isset($_POST["submit"]))
		{
            
            $target_book_dir = "books/";
            $target_book_file = $target_book_dir . basename($_FILES["book"]["name"]);
            // echo $target_file;
			if (move_uploaded_file($_FILES["book"]["tmp_name"], $target_book_file))
			{
                $target_cover_dir = "books/cover/";
                $target_cover_file = $target_cover_dir . basename($_FILES["coverimage"]["name"]);
                if (move_uploaded_file($_FILES["coverimage"]["tmp_name"], $target_cover_file))
                {
                    $name=$_POST["name"];
                    $dept=$_POST["dept"];
                    $year=$_POST["year"];
                    $author=$_POST["author"];
                    $keywords=$_POST["keywords"];
                    $sql="INSERT INTO book (BOOK_NAME, BOOK_DEPT, BOOK_YEAR, BOOK_AUTHOR, BOOK_IMAGE, BOOK_URL, BOOK_UPLOADER, TIMEANDDATE, BOOK_KEYWORDS,TYPES) VALUES ('$name', '$dept', '$year', '$author', '$target_cover_file', '$target_book_file', '$admin_name', NOW(), '$keywords','FILE');";  
                      if($db->query($sql))
				        {
		                     echo "<br><div class='alert alert-success alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>The Book $name Added succesfully</strong>
                        </div><br>";
                        }          
                }
			}
					
			else
			{
				  echo "<br><div class='alert alert-danger alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Error Uploading the File</strong>
                        </div><br>";
			}
	
		}
?>
                <form action="" autocomplete="off" method="post" enctype="multipart/form-data">
                 <legend>Book</legend>
                    <div class="form-group"> 
                        <label for="name">Book Name:</label>
                        <input type="text" required name="name" class="form-control" placeholder="Eg: Theory of Computation" id="">
                    </div> 
                    <div class="form-group"> 
                        <label for="dept">Department</label>
                        <select name="dept" class="form-control" placeholder="Eg: Theory of Computation" id="">
                            <option value="CSE">CSE</option>
                            <option value="BIO">BIO</option>
                            <option value="ECE">ECE</option>
                        </select>
                    </div> 
                    <div class="form-group"> 
                        <label for="year">Year</label>
                        <select name="year" class="form-control" placeholder="Eg: Compiler Design" id="">
                            <option value="First Year">I<sup>st</sup> Year</option>
                            <option value="Second Year">II<sup>nd</sup> Year</option>
                            <option value="Third Year">III<sup>rd</sup> Year</option>
                            <option value="Final Year">IV<sup>th</sup> Year</option>
                       </select>
                    </div> 
                    <div class="form-group"> 
                        <label for="author">Author Name: (optional)</label>
                        <input type="text" name="author" class="form-control" placeholder="Eg: J. Stanley Jayaprakash" id="">
                    </div> 
                    <div class="form-group"> 
                        <label for="coverimage">Book Cover  Image (optional)</label>
                        <input type="file" name="coverimage" class="form-control">
                    </div> 
                    <div class="form-group"> 
                        <label for="book">Book </label>
                        <input type="file" name="book" class="form-control" required>
                    </div> 
                    <div class="form-group"> 
                        <label for="keywords">Keywords </label>
                        <textarea type="text" name="keywords" class="form-control" required></textarea>
                    </div> 
                
                        <input type="submit" name="submit" class="btn btn-block btn-success" value="add book">
               
                </form>
                
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
   

</body>


</html>