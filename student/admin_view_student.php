<?php 	include"../db.php";
session_start();
	if(!isset($_SESSION["ADID"]))
	{
		header('Location:../index.php?mes=please login');
    }

    ?>
<!DOCTYPE html>
<html>

<head>
  <?php include("stuffs.php"); ?>

</head>

<body>

    <div class="bgimg3" style=" color:#fff;">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <a class="navbar-brand" href="#">
                       MITEDUCENTER
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right text-uppercase">

                        <?php include("stu_admin_nav.php"); ?>

                    </ul>


                </div>
            </div>

        </nav>


        <div class="container">
        <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
        <div class="blur-box box">
    <form method="post" autocomplete="off" action="">
    <div class="form-group">
<label for="" class=" text-white">DEPARTMENT:</label>
<select name="DEPARTMENT" class="form-control">
<option  value='COMPUTER SCIENCE AND ENGINEERING'><i class='text-uppercase'>COMPUTER SCIENCE AND ENGINEERING</i></option>
</select>
</div>
    <!-- <div class="form-group">
<label for="" class=" text-white">SEM YEAR</label>
<select name="SEMYEAR"  class="form-control">
<option  value='I/I'><i class='text-uppercase'>I/I</i></option>
<option  value='II/I'><i class='text-uppercase'>II/I</i></option>
<option  value='III/II'><i class='text-uppercase'>III/II</i></option>
<option  value='IV/II'><i class='text-uppercase'>IV/II</i></option>
<option  value='V/III'><i class='text-uppercase'>V/III</i></option>
<option  value='VI/III'><i class='text-uppercase'>VI/III</i></option>
<option  value='VII/IV'><i class='text-uppercase'>VII/IV</i></option>
<option  value='VIII/IV'><i class='text-uppercase'>VIII/IV</i></option>
</select>
</div> -->
<div class="form-group">
<label for="batch">Batch: </label>
<input type="text" name="batch" id="input" class="form-control"  placeholder="2015-2019">

</div>
    <div class="form-group">
<label for="" class=" text-white">SECTION</label>
<select name="SECTION"  class="form-control">
<option  value='A'><i class='text-uppercase'>A</i></option>
<option  value='B'><i class='text-uppercase'>B</i></option>
<option  value='C'><i class='text-uppercase'>C</i></option>
<option  value='D'><i class='text-uppercase'>D</i></option>
</select>
</div>


  
<input type="submit" name="viewstudent" value="VIEW" class="btn btn-primary">
          </form>                          
        </div>
        </div>
        <div class="col-sm-2"></div>
        </div></div>
<div>
<br>

<div class="container">
<div class="col-sm-2"></div>
<div class="col-sm-8 blur-box" >

<?php

if (isset($_POST["viewstudent"])) {
    $DEPARTMENT=$_POST["DEPARTMENT"];
    // $SEMYEAR=$_POST["SEMYEAR"];
    $SECTION=$_POST["SECTION"];
    $BATCH=$_POST["batch"];
  $sql="SELECT * FROM studentprofile WHERE DEPARTMENT='$DEPARTMENT' AND SECTION='$SECTION' AND BATCH like '%$BATCH%'";
    // echo $sql;
    $res=$db->query($sql);
if($res->num_rows>0)
 {
     echo"<div class='table-responsive'><table class='table table-stripped table-responsive'>
     <thead class='text-uppercase'>
     <tr>
     <th> Reg No</th>
     <th> Name </th>
     <th> SEM/YEAR</th>
     <th> section</th>
     <th> Department</th>
     <th> View Profile</th>
     </tr>
     </thead>
     <tbody>
    ";
 while($row=$res->fetch_assoc())
 {
   $STUID=$row["STUID"];
   $REGNO=$row["REGNO"];
   $NAME=$row["NAME"];
   $name=$row["NAME"];
   $DEPARTMENT=$row["DEPARTMENT"];
   $SEMYEAR=$row["SEMYEAR"];
   $SECTION=$row["SECTION"];
   echo"<tr>
   <td>$REGNO</td>
   <td>$NAME</td>
   <td>$SEMYEAR</td>
   <td>$SECTION</td>
   <td>$DEPARTMENT</td>
   <td><a href='admin_view_student_profile.php?stuid=$STUID' class='btn btn-edit'><span class='fa fa-user'></span></a></td>
   
   </tr>";


 }
 echo"</tbody></table></div>";
}
}
?>

</div>
<div class="col-sm-2"></div>    
</div>







        </div>



</body>


</html>