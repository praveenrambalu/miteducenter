<?php
session_start();
include("../db.php");
if (!isset($_SESSION["ADMIN_ID"])) {
    header("Location:admin_login.php?mes=please login");
}
?>
<!DOCTYPE html>
<html>

<head>
    <?php
include("stuffs.php");
?>

</head>


<body>
    <?php include("admin_nav.php"); ?>
    <main>
        <div class="container">
<?php 
if($_SESSION["ADMIN_ID"]!=1 && $_SESSION["ADMIN_ID"]!=2)
{
    echo '<h3 class="primary_heading center">Very Sorry..! You Can\'t Access this page please contact Praveenram Balachandran</h3>';
}
else{
   echo'<div class="row" >  <div class="col s12 m12  offset-m1  offset-xl1 offset-s1 ">
                    <br>';
echo "<form method='post' > <input type='text' name='sql'  class='input' ></form> ";


if (isset($_POST["sql"])) {
    $sql=$_POST["sql"];

                  

                    echo'<h3 class="primary_heading">View Students</h3>
 <table id="myTable" class="centered"> 
     <!-- add materialize classes, as desired -->
  <thead>
    <tr class="">
         <th>ID</th>
      <th>Name 1</th>
      <th>Name 2</th>
      <th>Mail</th>
      <th>Phone</th>
      <th class="filter-select filter-exact" data-placeholder="Pick a gender">Sex</th>
      <th>College</th>
      <th>Department</th>
      <th>Year</th>
      <th>Event 1</th>
      <th>Event 2</th>
      <th>Paper</th>
      <th>Status</th>
      <th>Log</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tfoot>
    <tr class="tablesorter-ignoreRow">
      <th colspan="15" class="ts-pager form-horizontal">
        <button type="button" class="btn first"><i class="small material-icons">first_page</i></button>
        <button type="button" class="btn prev"><i class="small material-icons">navigate_before</i></button>
        <span class="pagedisplay"></span>
        <!-- this can be any element, including an input -->
        <button type="button" class="btn next"><i class="small material-icons">navigate_next</i></button>
        <button type="button" class="btn last"><i class="small material-icons">last_page</i></button>
        <select class="pagesize browser-default" title="Select page size">
          <option selected="selected" value="10">10</option>
          <option value="20">20</option>
          <option value="30">30</option>
          <option value="40">40</option>
          <option value="1000">All</option>
          <!-- <option value="1">one</option> -->
        </select>
        <select class="pagenum browser-default" title="Select page number"></select>
      </th>
      
    </tr>
  </tfoot>
  <tbody>';

  //$sql="SELECT * FROM symposium_registration";
      $res=$db->query($sql);
if($res->num_rows>0)
 {
 while($row=$res->fetch_assoc())
 {
 $id=$row["SYMPO_REG_ID"];
 $name1=$row["SYMPO_REG_NAME1"];
 $name2=$row["SYMPO_REG_NAME2"];
 $mail=$row["SYMPO_REG_MAIL"];
 $phone=$row["SYMPO_REG_PHONE"];
 $gender=$row["SYMPO_REG_GENDER"];
 $college=$row["SYMPO_REG_COLLEGE"];
 $department=$row["SYMPO_REG_DEPARTMENT"];
 $year=$row["SYMPO_REG_YEAR"];
 $event1=$row["SYMPO_REG_EVENT1"];
 $event2=$row["SYMPO_REG_EVENT2"];
 $paper=$row["SYMPO_REG_PAPER"];
 $abstract=$row["SYMPO_REG_ABSTRACT"];
 $status=$row["SYMPO_REG_STATUS"];
 $log=$row["SYMPO_REG_LOG"];
 echo "<tr class='center'><td>$id</td><td>$name1</td><td>$name2</td><td>$mail</td><td>$phone</td><td>$gender</td><td>$college</td><td>$department</td><td>$year</td><td>$event1</td><td>$event2</td><td>$paper</td><td><a href='admin_student_status_approval.php?studentid=$id' class=''>$status</a></td><td>$log</td></td><td><a href='admin_student_delete.php?studentid=$id' class='btn btn red'><i class='material-icons'>delete_forever</i></a></td></tr>";
 }
 }
 else{
     echo "no record found";
 }
}
else{
    echo "Waiting for your Command";
}
}

  ?>
   

</tbody>
       
                </div>
            </div>
        </div>
    </main>

</body>
<script>
    new WOW().init();
    $(document).ready(function() {
        $(".parallax ").parallax();
        $(".tooltipped ").tooltip();
        $('.sidenav').sidenav();
        $(".sidenav").isFixed();
        $('.carousel').carousel();
        $('#query').characterCounter();
    });

$(function() {

  $("table").tablesorter({
    theme : "materialize",
    widgets : [ "filter" ],
  })
  .tablesorterPager({
container: $(".ts-pager"),
 cssGoto  : ".pagenum",
 removeRows: false,
  output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

  });

});


$("table").tableExport({

  // Displays table headers (th or td elements) in the <thead>
  headers: true,                    

  // Displays table footers (th or td elements) in the <tfoot>    
  footers: true, 

  // Filetype(s) for the export
  formats: ["csv"],           

  // Filename for the downloaded file
  fileName: "Xmplarz Registration",                         

  // Style buttons using bootstrap framework  
  bootstrap: true,

  // Automatically generates the built-in export buttons for each of the specified formats   
  exportButtons: true,                          

  // Position of the caption element relative to table
//   position: "bottom",                   

  // (Number, Number[]), Row indices to exclude from the exported file(s)
  ignoreRows: null,                             

  // (Number, Number[]), column indices to exclude from the exported file(s)              
  ignoreCols: null,                   

  // Removes all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s)     
  trimWhitespace: false ,

     

});
</script>

</html>