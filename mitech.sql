-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2018 at 10:05 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mitech`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `BOOK_ID` int(11) NOT NULL,
  `BOOK_NAME` text NOT NULL,
  `BOOK_DEPT` text NOT NULL,
  `BOOK_YEAR` varchar(20) NOT NULL,
  `BOOK_AUTHOR` text NOT NULL,
  `BOOK_IMAGE` text NOT NULL,
  `BOOK_URL` text NOT NULL,
  `BOOK_UPLOADER` text NOT NULL,
  `TIMEANDDATE` text NOT NULL,
  `BOOK_KEYWORDS` text NOT NULL,
  `TYPES` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`BOOK_ID`, `BOOK_NAME`, `BOOK_DEPT`, `BOOK_YEAR`, `BOOK_AUTHOR`, `BOOK_IMAGE`, `BOOK_URL`, `BOOK_UPLOADER`, `TIMEANDDATE`, `BOOK_KEYWORDS`, `TYPES`) VALUES
(5, 'Compiler Design', 'CSE', 'Third Year', 'J. Stanly Jaya prakash', 'books/cover/IMG-20180717-WA0013.jpg', 'books/FZ.docx', 'Praveenram', '2018-11-03 18:12:08', 'key wrods for the testing CSE', 'FILE'),
(6, 'Compiler Design', 'CSE', 'Third Year', 'J. Stanly Jaya prakash', 'books/cover/IMG-20180717-WA0013.jpg', 'books/FZ.docx', 'Praveenram', '2018-11-03 18:12:39', 'key wrods for the testing CSE', 'FILE'),
(7, 'Compiler Design', 'CSE', 'Third Year', 'J. Stanly Jaya prakash', 'books/cover/2.jpg', 'https://drive.google.com/file/d/10lsby-QxcB3NRq6PpI6FmST0LzVKhYhs', 'Praveenram', '2018-11-03 18:13:34', 'key wrods for the testingCSE', 'LINK'),
(8, 'desdfads', 'CSE', 'Final Year', '', 'books/cover/1.jpg', 'http://www.mediafire.com/download/f60kuo6dc5oqavk/GE6151_qb.pdf', 'Praveenram', '2018-11-03 18:15:47', 'nm prakash how are you bitchc CSE', 'LINK'),
(9, 'SOA SOFTWARE ORIENTED ARCHITECTURE CS6702', 'CSE', 'Final Year', 'ANNA UNIVERSITY', 'books/cover/2016-11-23-11-02-30-312.jpg', 'books/104753_2013_regulation.pdf', 'Praveenram', '2018-11-15 14:52:01', 'SOA SOFTWARE ORIENTED ARCHITECTURE CS6702 SOA SOF CSE', 'FILE'),
(10, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:15', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK'),
(11, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:20', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK'),
(12, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:22', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK'),
(13, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:24', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK'),
(14, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:25', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK'),
(15, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:26', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK'),
(16, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:26', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK'),
(17, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:27', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK'),
(18, 'test', 'CSE', 'First Year', 'test', 'books/cover/1.jpg', 'test', 'Praveenram', '2018-11-15 19:49:28', 'sdjfk sdakfjka dsfkfjkdsf asdjfkajs adkfj', 'LINK');

-- --------------------------------------------------------

--
-- Table structure for table `bookcomments`
--

CREATE TABLE `bookcomments` (
  `COMMENT_ID` int(11) NOT NULL,
  `COMMENT_STUDENT_NAME` text NOT NULL,
  `COMMENT_BOOK_ID` varchar(30) NOT NULL,
  `COMMENT_BOOK_NAME` varchar(30) NOT NULL,
  `COMMENT_LOG` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `COMMENT_STATUS` varchar(30) NOT NULL DEFAULT 'NOT-VIEWED',
  `COMMENT_COMMENTS` text NOT NULL,
  `COMMENT_WHO_CHANGED` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bookcomments`
--

INSERT INTO `bookcomments` (`COMMENT_ID`, `COMMENT_STUDENT_NAME`, `COMMENT_BOOK_ID`, `COMMENT_BOOK_NAME`, `COMMENT_LOG`, `COMMENT_STATUS`, `COMMENT_COMMENTS`, `COMMENT_WHO_CHANGED`) VALUES
(2, 'praveenrambalu', '9', 'sosa', '2018-11-15 16:15:00', 'VIEWED', 'hii how are you', 'Praveenram'),
(3, 'praveenrambalu', '9', 'soa', '2018-11-15 16:20:01', 'VIEWED', 'how are you', 'Praveenram'),
(4, 'PRAVEENRAMBALU', '9', 'sosa', '2018-11-15 16:54:42', 'VIEWED', 'HIII', 'Praveenram'),
(11, 'Praveenram', '8', 'des', '2018-11-15 17:03:11', 'VIEWED', 'COMMENTS', 'Praveenram'),
(19, 'Praveenram <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer\" x=\"0px\" y=\"0px\" width=\"20\" height=\"20\" viewBox=\"0 0 64 64\" enable-background=\"new 0 0 64 64\" xml:space=\"preserve\">\r\n<circle fill=\"#CCE4FF\" cx=\"32\" cy=\"32\" r=\"16\"/>\r\n<path fill=\"#007AFF\" d=\"M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z\"/>\r\n<path fill=\"#007AFF\" d=\"M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z\"/>\r\n<path fill=\"#007AFF\" d=\"M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z\"/>\r\n</svg>  ', '9', 'SOA SOFTWARE ORIENTED ARCHITEC', '2018-11-15 18:58:50', 'VIEWED', 'verified account\r\n', 'Praveenram'),
(20, 'Aswath s', '6', 'Compiler Design', '2018-11-15 19:06:37', 'VIEWED', 'very nice book', 'Praveenram'),
(21, 'prakash A', '9', 'SOA SOFTWARE ORIENTED ARCHITEC', '2018-11-15 21:18:25', 'VIEWED', 'hi mapla nice book', 'Praveenram'),
(22, 'Praveenram', '9', 'SOA SOFTWARE ORIENTED ARCHITEC', '2018-11-16 15:15:14', 'NOT-VIEWED', 'hi der\r\n', ''),
(23, 'sivaraja', '9', 'SOA SOFTWARE ORIENTED ARCHITEC', '2018-11-17 13:37:23', 'NOT-VIEWED', 'Worst subject and teaching mam', ''),
(24, 'Shanmugam', '9', 'SOA SOFTWARE ORIENTED ARCHITEC', '2018-11-17 14:07:35', 'NOT-VIEWED', 'Nice', '');

-- --------------------------------------------------------

--
-- Table structure for table `eventdetail`
--

CREATE TABLE `eventdetail` (
  `EVID` int(11) NOT NULL,
  `EV_NAME` text NOT NULL,
  `EV_DATE` text NOT NULL,
  `EV_DESCRIPTION` text NOT NULL,
  `EV_TIME` text NOT NULL,
  `EV_ORGANIZER` text,
  `EV_RESOURCEPERSON` text,
  `EV_COORDINATOR` text,
  `EV_CLASS` text,
  `EV_VENUE` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `eventdetail`
--

INSERT INTO `eventdetail` (`EVID`, `EV_NAME`, `EV_DATE`, `EV_DESCRIPTION`, `EV_TIME`, `EV_ORGANIZER`, `EV_RESOURCEPERSON`, `EV_COORDINATOR`, `EV_CLASS`, `EV_VENUE`) VALUES
(1, 'PYTHON PROGRAMMING', '2018-09-15', 'DESCRIPTION OF PYTHON PROGRAMMING', '09:01', 'CSE', 'SANJAIBAL', 'aswath', '3rd year cse', 'seminor hall-2'),
(2, 'chess game', '2018-11-03', 'game for the childds ', '00:00', 'praveenram', 'resourdce person', 'aswath', 'cladsafss', 'seminor hall-2'),
(3, 'Workshop for mechanical', '2018-11-17', 'mechanaicadl fajsdff kajksd', '10:00', 'alagu', 'sivaraja', 'alagu', 'mechanical', 'seminar hall');

-- --------------------------------------------------------

--
-- Table structure for table `eventquestions`
--

CREATE TABLE `eventquestions` (
  `QUID` int(11) NOT NULL,
  `EVENT_ID` text NOT NULL,
  `QUESTION` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `eventquestions`
--

INSERT INTO `eventquestions` (`QUID`, `EVENT_ID`, `QUESTION`) VALUES
(1, '1', 'The content was as described in publicity materials'),
(2, '1', 'The workshop was applicable to my  job'),
(3, '1', 'The program was well paced within the allotted time'),
(4, '1', 'Workshop Preferred level'),
(5, '1', 'In your opinion, was this workshop'),
(6, '1', 'The instructor was knowledgeable on the topic'),
(7, '1', '\r\nHow do you feel about this workshop'),
(8, '1', 'Skills development in the area of and group therapy'),
(9, '1', 'Teaching aids/audiovisuals were used effectively'),
(10, '1', 'Teaching style was effective'),
(11, '2', 'how was the game'),
(12, '2', 'how are you'),
(13, '2', 'how is the game'),
(14, '3', 'how is the workshop '),
(15, '3', 'how is his performance\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `facultyprofile`
--

CREATE TABLE `facultyprofile` (
  `FAID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `facultyprofile`
--

INSERT INTO `facultyprofile` (`FAID`, `NAME`, `USERNAME`, `PASSWORD`) VALUES
(1, 'Praveenram', 'praveenrambalu', 'a63208d97ee31c052f0802e19301936f');

-- --------------------------------------------------------

--
-- Table structure for table `fbans`
--

CREATE TABLE `fbans` (
  `FBID` int(11) NOT NULL,
  `FB_EVENTID` text NOT NULL,
  `FB_QUID` text NOT NULL,
  `FB_ANS` text NOT NULL,
  `FB_STUID` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fbans`
--

INSERT INTO `fbans` (`FBID`, `FB_EVENTID`, `FB_QUID`, `FB_ANS`, `FB_STUID`) VALUES
(1, '1', '1', '4', '1'),
(2, '1', '2', '3', '1'),
(3, '1', '3', '3', '1'),
(4, '1', '4', '3', '1'),
(5, '1', '5', '4', '1'),
(6, '1', '6', '5', '1'),
(7, '1', '7', '2', '1'),
(8, '1', '8', '2', '1'),
(9, '1', '9', '3', '1'),
(10, '1', '10', '4', '1'),
(11, '1', '1', '5', '3'),
(12, '1', '2', '4', '3'),
(13, '1', '3', '3', '3'),
(14, '1', '4', '2', '3'),
(15, '1', '5', '2', '3'),
(16, '1', '6', '2', '3'),
(17, '1', '7', '2', '3'),
(18, '1', '8', '3', '3'),
(19, '1', '9', '3', '3'),
(20, '1', '10', '4', '3'),
(21, '1', '1', '5', '2'),
(22, '1', '2', '3', '2'),
(23, '1', '3', '4', '2'),
(24, '1', '4', '3', '2'),
(25, '1', '5', '3', '2'),
(26, '1', '6', '4', '2'),
(27, '1', '7', '4', '2'),
(28, '1', '8', '4', '2'),
(29, '1', '9', '5', '2'),
(30, '1', '10', '5', '2'),
(31, '1', '1', '5', '1'),
(32, '1', '2', '4', '1'),
(33, '1', '3', '4', '1'),
(34, '1', '4', '4', '1'),
(35, '1', '5', '4', '1'),
(36, '1', '6', '4', '1'),
(37, '1', '7', '4', '1'),
(38, '1', '8', '4', '1'),
(39, '1', '9', '4', '1'),
(40, '1', '10', '4', '1'),
(41, '1', '1', '5', '5'),
(42, '1', '2', '4', '5'),
(43, '1', '3', '4', '5'),
(44, '1', '4', '4', '5'),
(45, '1', '5', '4', '5'),
(46, '1', '6', '4', '5'),
(47, '1', '7', '5', '5'),
(48, '1', '8', '4', '5'),
(49, '1', '9', '5', '5'),
(50, '1', '10', '5', '5'),
(51, '2', '11', '5', '1'),
(52, '2', '12', '4', '1'),
(53, '2', '11', '5', '5'),
(54, '2', '12', '1', '5'),
(55, '3', '14', '5', '1'),
(56, '3', '15', '4', '1'),
(57, '3', '14', '4', '5'),
(58, '3', '15', '2', '5'),
(59, '1', '1', '1', '6'),
(60, '1', '2', '1', '6'),
(61, '1', '3', '2', '6'),
(62, '1', '4', '1', '6'),
(63, '1', '5', '1', '6'),
(64, '1', '6', '1', '6'),
(65, '1', '7', '1', '6'),
(66, '1', '8', '1', '6'),
(67, '1', '9', '1', '6'),
(68, '1', '10', '1', '6'),
(69, '1', '1', '4', '7'),
(70, '1', '2', '3', '7'),
(71, '1', '3', '4', '7'),
(72, '1', '4', '3', '7'),
(73, '1', '5', '4', '7'),
(74, '1', '6', '5', '7'),
(75, '1', '7', '4', '7'),
(76, '1', '8', '4', '7'),
(77, '1', '9', '3', '7'),
(78, '1', '10', '5', '7');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `MSG_ID` int(11) NOT NULL,
  `USER_ID` int(10) NOT NULL,
  `MSG` text NOT NULL,
  `MSG_LOG` text NOT NULL,
  `SENDER_NAME` text NOT NULL,
  `MSG_PHOTO` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`MSG_ID`, `USER_ID`, `MSG`, `MSG_LOG`, `SENDER_NAME`, `MSG_PHOTO`) VALUES
(1, 6, 'Hii', '2018-11-17 13:38:15', 'sivaraja', ''),
(2, 6, 'Hello', '2018-11-17 13:38:19', 'sivaraja', ''),
(3, 6, 'Hii', '2018-11-17 13:38:22', 'sivaraja', ''),
(4, 6, 'Hii', '2018-11-17 13:38:26', 'sivaraja', ''),
(5, 6, 'Hii', '2018-11-17 13:38:33', 'sivaraja', ''),
(6, 1, 'hi siva', '2018-11-17 13:38:34', 'Praveenram<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer\" x=\"0px\" y=\"0px\" width=\"20\" height=\"20\" viewBox=\"0 0 64 64\" enable-background=\"new 0 0 64 64\" xml:space=\"preserve\">\r\n<circle fill=\"#CCE4FF\" cx=\"32\" cy=\"32\" r=\"16\"/>\r\n<path fill=\"#007AFF\" d=\"M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z\"/>\r\n<path fill=\"#007AFF\" d=\"M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z\"/>\r\n<path fill=\"#007AFF\" d=\"M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z\"/>\r\n</svg>  ', '../img/profile/admin.png'),
(7, 6, 'Hii', '2018-11-17 13:38:35', 'sivaraja', ''),
(8, 6, 'Hii', '2018-11-17 13:38:38', 'sivaraja', ''),
(9, 6, 'Hii', '2018-11-17 13:38:41', 'sivaraja', ''),
(10, 1, 'ena pandra', '2018-11-17 13:38:50', 'Praveenram<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer\" x=\"0px\" y=\"0px\" width=\"20\" height=\"20\" viewBox=\"0 0 64 64\" enable-background=\"new 0 0 64 64\" xml:space=\"preserve\">\r\n<circle fill=\"#CCE4FF\" cx=\"32\" cy=\"32\" r=\"16\"/>\r\n<path fill=\"#007AFF\" d=\"M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z\"/>\r\n<path fill=\"#007AFF\" d=\"M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z\"/>\r\n<path fill=\"#007AFF\" d=\"M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z\"/>\r\n</svg>  ', '../img/profile/admin.png'),
(11, 1, 'sivaraja', '2018-11-17 13:38:53', 'Praveenram<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer\" x=\"0px\" y=\"0px\" width=\"20\" height=\"20\" viewBox=\"0 0 64 64\" enable-background=\"new 0 0 64 64\" xml:space=\"preserve\">\r\n<circle fill=\"#CCE4FF\" cx=\"32\" cy=\"32\" r=\"16\"/>\r\n<path fill=\"#007AFF\" d=\"M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z\"/>\r\n<path fill=\"#007AFF\" d=\"M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z\"/>\r\n<path fill=\"#007AFF\" d=\"M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z\"/>\r\n</svg>  ', '../img/profile/admin.png'),
(12, 1, 'saptiya', '2018-11-17 13:38:58', 'Praveenram<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer\" x=\"0px\" y=\"0px\" width=\"20\" height=\"20\" viewBox=\"0 0 64 64\" enable-background=\"new 0 0 64 64\" xml:space=\"preserve\">\r\n<circle fill=\"#CCE4FF\" cx=\"32\" cy=\"32\" r=\"16\"/>\r\n<path fill=\"#007AFF\" d=\"M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z\"/>\r\n<path fill=\"#007AFF\" d=\"M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z\"/>\r\n<path fill=\"#007AFF\" d=\"M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z\"/>\r\n</svg>  ', '../img/profile/admin.png'),
(13, 1, 'Hii', '2018-11-17 13:39:57', 'Praveenram', '../img/profile/611615104066.jpg'),
(14, 7, 'Hi', '2018-11-17 14:08:21', 'Shanmugam', ''),
(15, 7, 'H r u ...?', '2018-11-17 14:08:31', 'Shanmugam', ''),
(16, 1, 'fine julie you', '2018-11-17 14:08:39', 'Praveenram<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer\" x=\"0px\" y=\"0px\" width=\"20\" height=\"20\" viewBox=\"0 0 64 64\" enable-background=\"new 0 0 64 64\" xml:space=\"preserve\">\r\n<circle fill=\"#CCE4FF\" cx=\"32\" cy=\"32\" r=\"16\"/>\r\n<path fill=\"#007AFF\" d=\"M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z\"/>\r\n<path fill=\"#007AFF\" d=\"M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z\"/>\r\n<path fill=\"#007AFF\" d=\"M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z\"/>\r\n</svg>  ', '../img/profile/admin.png'),
(17, 7, 'Good job keep it up buddy', '2018-11-17 14:08:48', 'Shanmugam', ''),
(18, 1, 'tq dear', '2018-11-17 14:08:53', 'Praveenram<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer\" x=\"0px\" y=\"0px\" width=\"20\" height=\"20\" viewBox=\"0 0 64 64\" enable-background=\"new 0 0 64 64\" xml:space=\"preserve\">\r\n<circle fill=\"#CCE4FF\" cx=\"32\" cy=\"32\" r=\"16\"/>\r\n<path fill=\"#007AFF\" d=\"M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z\"/>\r\n<path fill=\"#007AFF\" d=\"M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z\"/>\r\n<path fill=\"#007AFF\" d=\"M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z\"/>\r\n</svg>  ', '../img/profile/admin.png'),
(19, 7, 'Yeah fyn', '2018-11-17 14:08:56', 'Shanmugam', ''),
(20, 1, 'what are you doing', '2018-11-17 14:08:58', 'Praveenram<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"Layer\" x=\"0px\" y=\"0px\" width=\"20\" height=\"20\" viewBox=\"0 0 64 64\" enable-background=\"new 0 0 64 64\" xml:space=\"preserve\">\r\n<circle fill=\"#CCE4FF\" cx=\"32\" cy=\"32\" r=\"16\"/>\r\n<path fill=\"#007AFF\" d=\"M32,9C19.318,9,9,19.317,9,32s10.318,23,23,23s23-10.317,23-23S44.682,9,32,9z M32,53  c-11.58,0-21-9.421-21-21s9.42-21,21-21s21,9.421,21,21S43.58,53,32,53z\"/>\r\n<path fill=\"#007AFF\" d=\"M32,15c-9.374,0-17,7.626-17,17s7.626,17,17,17s17-7.626,17-17S41.374,15,32,15z M32,47  c-8.271,0-15-6.729-15-15s6.729-15,15-15s15,6.729,15,15S40.271,47,32,47z\"/>\r\n<path fill=\"#007AFF\" d=\"M37.168,25.445l-7.323,10.985l-3.138-3.138c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l4,4  C29.481,38.896,29.736,39,30,39c0.033,0,0.065-0.002,0.099-0.005c0.298-0.029,0.567-0.191,0.733-0.44l8-12  c0.306-0.46,0.182-1.08-0.277-1.387C38.095,24.862,37.474,24.985,37.168,25.445z\"/>\r\n</svg>  ', '../img/profile/admin.png'),
(21, 7, ' Lyg', '2018-11-17 14:09:03', 'Shanmugam', ''),
(22, 5, 'hi', '2018-11-17 14:14:18', 'prakash A', '../img/profile/no-profile.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `POST_ID` int(11) NOT NULL,
  `POST_TITLE` varchar(50) NOT NULL,
  `POST_DESCRIPTION` text NOT NULL,
  `POST_FROM` varchar(30) NOT NULL,
  `POST_TO` varchar(30) NOT NULL,
  `POST_VENUE` varchar(50) NOT NULL,
  `POST_CONTACT` varchar(20) NOT NULL,
  `POST_MAIL` text NOT NULL,
  `POST_INCHARGE` varchar(30) NOT NULL,
  `POST_UPLOADER` varchar(30) NOT NULL,
  `POST_LOG` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `POST_REGLINK` text NOT NULL,
  `POST_IMAGE` text NOT NULL,
  `POST_STATUS` varchar(20) NOT NULL DEFAULT 'ACTIVATED'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`POST_ID`, `POST_TITLE`, `POST_DESCRIPTION`, `POST_FROM`, `POST_TO`, `POST_VENUE`, `POST_CONTACT`, `POST_MAIL`, `POST_INCHARGE`, `POST_UPLOADER`, `POST_LOG`, `POST_REGLINK`, `POST_IMAGE`, `POST_STATUS`) VALUES
(2, 'Wipro- Campus Drive', 'Wipro Campus Drive for 2019 batch', '2018-11-17', '2018-11-19', 'Mec Barahiyar arangam', '9876543210', 'ram@ram.com', 'ram', 'Praveenram', '2018-11-16 07:01:43', 'http://praveenrambalu.tk', 'img/posts/2016-11-17-18-01-39-555.jpg', 'ACTIVATED'),
(7, 'error testing process', 'testing for the error no error found if the file has uploaded testing for the error no error found if the file has uploadedd testing for the error no error found if the file has uploaded', '2018-11-17', '2018-11-26', 'seminar hall', '', 'praveenrambalu@gmail.com', '', 'Praveenram', '2018-11-17 08:44:25', 'https://www.utmb.edu/pediedtech/PosterToolBox/poster.htm', 'https://www.utmb.edu/pediedtech/PosterToolBox_for_APA-ESP/poster1.jpg', 'ACTIVATED'),
(8, 'eeorore', ' dsaklfj', '0315-04-05', '4546-02-15', '15215', '5', '5', '5', 'Praveenram', '2018-11-17 08:48:47', 'http://dfkjladjsf.com', 'img/IMG_20170420_175541496.jpg', 'ACTIVATED'),
(9, 'ds', 'dsaf', '0045-04-05', '0565-05-06', '565', '565', '656@gmail.com', '', 'Praveenram', '2018-11-17 08:53:56', '', 'img/IMG_20161126_200502.jpg', 'ACTIVATED'),
(12, 'dsfad', 'dsfad', '0005-05-04', '0004-05-06', '55', '564', '56', '5', 'Praveenram', '2018-11-17 08:54:54', '', 'img/0.jpg', 'ACTIVATED'),
(27, 'dkslfjkdls', 'dsaskfjdsk', '65454-05-04', '0564-05-04', '56454', '564', '', '', 'Praveenram', '2018-11-17 08:59:15', '', 'post/img/5.jpg', 'ACTIVATED');

-- --------------------------------------------------------

--
-- Table structure for table `studentprofile`
--

CREATE TABLE `studentprofile` (
  `STUID` int(11) NOT NULL,
  `REGNO` varchar(30) NOT NULL,
  `ROLLNO` varchar(30) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `DOB` varchar(30) NOT NULL,
  `BLOODGROUP` varchar(10) DEFAULT NULL,
  `CONTACTNO` varchar(30) DEFAULT NULL,
  `MAILID` varchar(50) DEFAULT NULL,
  `PARENTCONTACTNO` varchar(30) DEFAULT NULL,
  `GENDER` varchar(6) NOT NULL,
  `DEPARTMENT` varchar(50) NOT NULL,
  `FIRSTGRADUATE` varchar(30) DEFAULT NULL,
  `CLASSADVISOR` varchar(30) NOT NULL,
  `MENTOR` varchar(30) NOT NULL,
  `SECTION` varchar(4) DEFAULT NULL,
  `DOJ` varchar(10) DEFAULT NULL,
  `SEMYEAR` varchar(15) NOT NULL,
  `FATHERNAME` varchar(30) NOT NULL,
  `FATHEROCCUPATION` varchar(30) DEFAULT NULL,
  `MOTHERNAME` varchar(30) NOT NULL,
  `MOTHEROCCUPATION` varchar(30) DEFAULT NULL,
  `ANNUALINCOME` varchar(30) DEFAULT NULL,
  `MOTHERTONGUE` varchar(10) DEFAULT NULL,
  `MEDIUM` varchar(15) DEFAULT NULL,
  `BOARD` varchar(15) DEFAULT NULL,
  `BATCH` varchar(10) NOT NULL,
  `ADMISSIONTYPE` varchar(10) NOT NULL,
  `QUOTA` varchar(10) NOT NULL,
  `PANNO` varchar(15) DEFAULT NULL,
  `AADHARNO` varchar(15) DEFAULT NULL,
  `HSCMARK` varchar(4) DEFAULT NULL,
  `HSCSCHOOL` text,
  `SSLCMARK` varchar(4) NOT NULL,
  `SSLCSCHOOL` text NOT NULL,
  `DIPLOMAMARK` varchar(4) DEFAULT NULL,
  `DIPLOMACOLLEGE` text,
  `RELIGION` varchar(10) NOT NULL,
  `NATIONALITY` varchar(10) NOT NULL,
  `COMMUNITY` varchar(25) DEFAULT NULL,
  `CASTE` varchar(25) DEFAULT NULL,
  `DOORNO` text,
  `STREETNAME` text,
  `PLACE` text,
  `TALUK` text,
  `DISTRICT` text,
  `PINCODE` text,
  `PHOTO` text,
  `SIGNATURE` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studentprofile`
--

INSERT INTO `studentprofile` (`STUID`, `REGNO`, `ROLLNO`, `NAME`, `DOB`, `BLOODGROUP`, `CONTACTNO`, `MAILID`, `PARENTCONTACTNO`, `GENDER`, `DEPARTMENT`, `FIRSTGRADUATE`, `CLASSADVISOR`, `MENTOR`, `SECTION`, `DOJ`, `SEMYEAR`, `FATHERNAME`, `FATHEROCCUPATION`, `MOTHERNAME`, `MOTHEROCCUPATION`, `ANNUALINCOME`, `MOTHERTONGUE`, `MEDIUM`, `BOARD`, `BATCH`, `ADMISSIONTYPE`, `QUOTA`, `PANNO`, `AADHARNO`, `HSCMARK`, `HSCSCHOOL`, `SSLCMARK`, `SSLCSCHOOL`, `DIPLOMAMARK`, `DIPLOMACOLLEGE`, `RELIGION`, `NATIONALITY`, `COMMUNITY`, `CASTE`, `DOORNO`, `STREETNAME`, `PLACE`, `TALUK`, `DISTRICT`, `PINCODE`, `PHOTO`, `SIGNATURE`) VALUES
(1, '611615104066', '315UCS066', 'Praveenram', '1998-06-04', 'O+', '8220085613', 'praveenrambalu@gmail.com', '9994939975', 'MALE', 'Computer Science and Engineering', 'YES', 'S.LATHA', 'S.LATHA', 'B', '2015-08-12', 'VII/IV', 'BALACHANDRAN', 'CLERK', 'BANUMATHI', 'HOUSE WIFE', '1,20,000', 'TAMIL', 'TAMIL', 'STATE', '2015-2019', 'REGULAR', 'GQ', '-', '4063 4511 4545', '769', 'GBHSS,VLN', '436', 'GHS,CSP', '', '', 'HINDU', 'INDIAN', 'BC', 'KAMMALAR', '2/62', 'WEST STREET', 'CHANDRASEKARAPURAM', 'VALANGAIMAN', 'THIRUVARUR', '612804', '../img/profile/611615104066.jpg', NULL),
(2, '611616104014', '316UCS014', 'Aswath s', '1999-07-16', 'O+', '9894210556', 'aswathshift@gmail.com', '8667789262', 'MALE', 'Computer Science and Engineering', 'YES', 'K.Savitha AP/CSE', 'K.Savitha AP/CSE', 'A', '2016-07-08', 'V/III', 'Seenivasan', 'Corpenter', 'Ponmalar', 'House wife', '100000', 'TAMIL', 'TAMIL', 'STATE', '2016-2020', 'REGULAR', 'GQ', 'COGPA0338K', '9566 6598 2568', '1010', 'C.G.M ', '429', 'S.R.D', '', '', 'HINDU', 'INDIAN', 'BC', 'Kammalar', '419', 'East street', 'Melkaraipatty', 'Palani', 'Dindugal', '624  617', '../img/profile/no-profile.jpg', NULL),
(3, '611616104093', '316UCS093', 'Sanjaibal D', '1998-06-24', 'B+', '7373966701', 'sanjaybal664@gmail.com', '8838887144', 'MALE', 'Computer Science and Engineering', 'YES', 'V.Kalayarasi AP/CSE', 'S.Priyanga AP/CSE', 'B', '2016-07-09', 'V/III', 'Dhanabal', 'Power Loom', 'Sangeetha', 'Power Loom', '100000', 'TAMIL', 'TAMIL', 'STATE', '2016-2020', 'REGULAR', 'MQ', 'ASPPA2425H', '2562 5655 5569', '980', 'M.D.V', '450', 'Govt.Boys', '', '', 'HINDU', 'INDIAN', 'SCA', 'Arunthathiyar', '280', 'AR street', 'Ammasipalayam', 'Kokkarayanpettai', 'Namakkal', '638 007', '../img/profile/no-profile.jpg', NULL),
(4, '611616104095', '316UCS095', 'Santhosh Kumar S', '1999-06-17', 'A+', '9629548639', 'sannthoshkumar@gmail.com', '8956485623', 'MALE', 'Computer Science and Engineering', 'YES', 'V.Kalayarasi AP/CSE', 'S.Priyanga AP/CSE', 'B', '2016-07-10', 'V/III', 'Saravanan', 'Silver', 'Krishnaveni', 'House wife', '20000', 'TAMIL', 'TAMIL', 'STATE', '2016-2020', 'REGULAR', 'MQ', 'ASSPA45856J', '5525 2562 2565', '1000', 'L.F', '410', 'L.F', '', '', 'HINDU', 'INDIAN', 'SC ', 'Aadidravidar', '858', 'Gorimadu', 'Selam', 'Selam', 'Selam', '633 002', '../img/profile/no-profile.jpg', NULL),
(5, '611615104063', '315UCS063', 'prakash A', '1997-05-02', 'AB+', '9943212453', 'dhassprakash5@gmail.com', '-', 'MALE', 'Computer Science and Engineering', 'YES', 'S. Latha', 'S. Latha', 'B', '2015-08-12', 'VII/IV', 'Aandiyappan', 'Farmer', 'Pavalakodi', 'Farmer', '36,000', 'Tamil', 'Tamil', 'STATE', '2015-2019', 'REGULAR', 'GQ', 'sdkaj 2f43a', '1212 1212 2121', '930', 'Vani Vikas Matric Hr. Sec. School', '398', 'St. Pauls Matric Hr. Sec. School', '', '', 'Hindu', 'Indian', 'ST', 'Malayali', '3/369', 'Vellikadu village', 'Karumandurai', 'P.N. Palayam', 'Salem', '636138', '../img/profile/no-profile.jpg', NULL),
(6, '611615104092', '315ucs092', 'sivaraja', '1998-04-05', 'o-', '9600971107', 'msivarjacse@gmail.com', '9786179132', 'MALE', 'Computer Science and Engineering', 'YES', 'S.Latha', 'B. Mohanraj', 'B', '2015-08-12', 'VII/IV', 'mariyappan', 'farmer', 'murugayi', 'farmer', '100000', 'tamil', 'TAMIL', 'STATE', '2015-2019', 'REGULAR', 'MQ', '-', '9273 4140 5810', '632', 'G.H.S, lakshmipuram', '354', 'G.H.S, lakshmipuram', '', '', 'hindu', 'indian', 'sc', 'paraiyar', '179/4', 'north street', 'saruthupatti po', 'periyakulam', 'theni', '625523', '../img/profile/no-profile.jpg', NULL),
(7, '611615104090', '315UCS090', 'Shanmugam', '1997-05-11', 'o+', '8508446108', 'shanmugammowgli@gmail.com', '8344478565', 'MALE', 'Computer Science and Engineering', 'YES', 'S.LATHA', 'B.MOHANRAJ', 'B', '2015-08-12', 'VII/IV', 'SEKAR V', 'FARMER', 'SHANTHI S', 'HOUSE WIFE', '70000', 'TAMIL', 'TAMIL', 'STATE', '2015-2019', 'REGULAR', 'MQ', '-', '1234 56548 865', '782', 'K.K.H.S, PARAMATHIVELU', '400', 'K.K.H.S, PARAMATHIVELU', '', '', 'HINDU', 'INDIAN', 'SC', 'mavilan', '198/135', 'north sreet', 'paramathivelur', 'paramathi', 'namakkal', '638182', '../img/profile/no-profile.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `SUB_ID` int(11) NOT NULL,
  `SUB_NAME` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`SUB_ID`, `SUB_NAME`) VALUES
(1, 'Theory of Computation'),
(2, 'google'),
(6, 'great indian sale'),
(7, 'Praveenram');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`BOOK_ID`);

--
-- Indexes for table `bookcomments`
--
ALTER TABLE `bookcomments`
  ADD PRIMARY KEY (`COMMENT_ID`);

--
-- Indexes for table `eventdetail`
--
ALTER TABLE `eventdetail`
  ADD PRIMARY KEY (`EVID`);

--
-- Indexes for table `eventquestions`
--
ALTER TABLE `eventquestions`
  ADD PRIMARY KEY (`QUID`);

--
-- Indexes for table `facultyprofile`
--
ALTER TABLE `facultyprofile`
  ADD PRIMARY KEY (`FAID`);

--
-- Indexes for table `fbans`
--
ALTER TABLE `fbans`
  ADD PRIMARY KEY (`FBID`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`MSG_ID`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`POST_ID`);

--
-- Indexes for table `studentprofile`
--
ALTER TABLE `studentprofile`
  ADD PRIMARY KEY (`STUID`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`SUB_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `BOOK_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `bookcomments`
--
ALTER TABLE `bookcomments`
  MODIFY `COMMENT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `eventdetail`
--
ALTER TABLE `eventdetail`
  MODIFY `EVID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eventquestions`
--
ALTER TABLE `eventquestions`
  MODIFY `QUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `facultyprofile`
--
ALTER TABLE `facultyprofile`
  MODIFY `FAID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fbans`
--
ALTER TABLE `fbans`
  MODIFY `FBID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `MSG_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `POST_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `studentprofile`
--
ALTER TABLE `studentprofile`
  MODIFY `STUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `SUB_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
